from django.contrib import admin
from barra.models import Permiso, Usuario, Venta, Articulo, Articulo_Venta, Deuda, Pago, Usuario_Permiso, Grupo

admin.site.register(Permiso)
admin.site.register(Usuario)
admin.site.register(Venta)
admin.site.register(Articulo)
admin.site.register(Articulo_Venta)
admin.site.register(Deuda)
admin.site.register(Pago)
admin.site.register(Usuario_Permiso)
admin.site.register(Grupo)
