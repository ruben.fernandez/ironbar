from django.conf.urls import patterns, url
from barra import views

urlpatterns = patterns("",
            url(r'^$', views.welcome, name='welcome'),
			url(r'^ventas$', views.ventas, name='ventas'),
			url(r'^facturacion$', views.facturacion, name='facturacion'),
            url(r'^ventaspendientes$', views.ventaspendientes, name='ventasPendientes'),
			url(r'^consultarventas$', views.consultarventas, name='consultarventas'),
			url(r'^welcome$', views.welcome, name='welcome'),
            url(r'^articulos$', views.articulos, name='articulos'),
		    url(r'^notas$', views.notas, name='notas'),
            url(r'^gestionUsuarios$', views.gestionUsuarios, name='gestionUsuarios'),
            url(r'^usuario$', views.usuario, name='usuario'),
			url(r'^permisos$', views.permisos, name='permisos'),
			url(r'^administracion$', views.administracion, name='administracion'),
			url(r'^gestionGrupos$', views.gestionGrupos, name='gestionGrupos')
		      )
