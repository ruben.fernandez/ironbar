# -*- coding: utf8 -*-
# Create your views here.

from barra.models import Venta
from barra.models import Deuda
from barra.models import Usuario
from barra.models import Permiso
from barra.models import Usuario_Permiso
from barra.models import Articulo
from barra.models import Articulo_Venta
from barra.models import Pago
from barra.models import Grupo

from django.shortcuts import render
from datetime import date

import datetime

import re
import hashlib
import codecs

def ventas(request):
  request.session['articuloCompleto'] = ""
  d = datetime.datetime.now()
  fecha = str(d.day) + "." + str(d.month) + "." + str(d.year)
  log = codecs.open("barra/static/log/log_"+fecha, "a", "utf-8")
  listaUsuarios = Usuario.objects.all()
  tiene_permiso = 0
  if request.session.has_key('permisos'):
    for nombre_permiso in request.session['permisos']:
      if (nombre_permiso == "Administrador") or (nombre_permiso == "Ventas"):
        tiene_permiso = 1
  if request.session.has_key('login') and request.session['login']!="" and tiene_permiso==1:
    total = 0.0
    cambio = 0.0
    entrega = 0.0
    cliente_bloqueado = "no"
    cliente_seleccionado = "Sin clasificar"
    cantidad = "1"
    cantidad_a_sumar = "1"
    articulo = ""
    mensaje = ""
    tipo_precio = "Normal"
    lista_clientes = []
    lista_busqueda = []
    cesta_compra = []

    #INIT
    if request.session.has_key('listaBusqueda') and request.session['listaBusqueda']!=[]:
      lista_busqueda = request.session['listaBusqueda']
    if request.session.has_key('cestaCompra') and request.session['cestaCompra']!=[]:
      cesta_compra = request.session['cestaCompra']
    if request.session.has_key('total') and request.session['total']!=0.0:
      total = request.session['total']
    if request.session.has_key('paga') and request.session['paga']!=0.0:
      entrega = request.session['paga']
    if request.session.has_key('cliente_bloqueado') and request.session['cliente_bloqueado']=="si":
      cliente_bloqueado = "si"
    if request.session.has_key('cliente_seleccionado') and request.session['cliente_seleccionado']!="Sin Clasificar":
      cliente_seleccionado = request.session['cliente_seleccionado']

    if request.POST.has_key('Limpiar'):
      request.session['listaBusqueda'] = []
      lista_busqueda = []
      request.session['cestaCompra'] = []
      cesta_compra = []
      request.session['total'] = 0.0
      total = 0.0
      request.session['paga'] = 0.0
      paga = 0.0
      request.session['cambio'] = 0.0
      cambio = 0.0
      request.session['cliente_bloqueado'] = "no"
      request.session['cliente_seleccionado'] = "Sin Clasificar"
      cliente_bloqueado = "no"
      tipo_precio = "Normal"

    #CLIENTE
    lista_clientes.append('Sin clasificar')
    for usuarioDB in listaUsuarios:
      lista_clientes.append(usuarioDB.login)

    if request.POST.has_key('filtrarUsuario') and request.POST.has_key('filtroUsuario') and request.POST['filtroUsuario']!="":
      lista_clientes = []
      regex = re.compile(request.POST['filtroUsuario'], re.IGNORECASE)
      lista_clientes.append('Sin clasificar')
      for usuarioDB in listaUsuarios:
        if re.search(regex, usuarioDB.login):
          lista_clientes.append(usuarioDB.login)

    #BUSCAR
    if request.POST.has_key('Buscar') and request.POST.has_key('busqueda'):
      lista_clientes = []
      cliente_bloqueado = "si"
      if request.session.has_key('cliente_bloqueado') and request.session['cliente_bloqueado'] == "no":
        lista_clientes.append(request.POST['cliente'])
        request.session['cliente_bloqueado'] = "si"
        cliente_seleccionado = request.POST['cliente']
        request.session['cliente_seleccionado'] = request.POST['cliente']
      try:
        tipo_precio = Usuario.objects.get(login=cliente_seleccionado).id_grupo.tipo_precio
      except:
        tipo_precio = "Normal"
      lista_busqueda = []
      listaArticulos = Articulo.objects.filter(oculto="0")
      for articulo in listaArticulos:
        expresion = request.POST['busqueda']
        if tipo_precio == "Normal":
          precio = articulo.precio
        elif tipo_precio == "Reducido":
          precio = articulo.precio2
        elif tipo_precio == "Coste":
          precio = articulo.precio3
        if articulo.codigo_articulo == expresion:
          tupla = (articulo.nombre_articulo, precio, str(articulo.id))
          lista_busqueda.append(tupla)
        else:
          regex = re.compile(expresion, re.IGNORECASE)
          if re.search(regex, articulo.nombre_articulo):
            tupla = (articulo.nombre_articulo, precio, str(articulo.id))
            lista_busqueda.append(tupla)
      request.session['listaBusqueda'] = lista_busqueda

    #ANIADIR AL CARRO
    if request.POST.has_key('aniadirAlCarro') and request.POST.has_key('scroll_box_izq'):
      try:
        if request.POST.has_key('cantidad') and request.POST['cantidad']!="":
          int(request.POST['cantidad'])
          cantidad = request.POST['cantidad']
          cantidad_a_sumar = request.POST['cantidad']
        articulo = request.POST['scroll_box_izq']
        id_articulo = articulo.split("|")[2]
        precio = articulo.split("|")[1]
        articulo = articulo.split("|")[0]
        for item in cesta_compra:
          if item[0]==articulo:
            tupla = (articulo, item[1], precio, id_articulo)
            cesta_compra.remove(tupla)
            cantidad = str(int(cantidad) + int(item[1]))
        tupla = articulo, cantidad, precio, id_articulo
        cesta_compra.append(tupla)
        total = total + float(precio) * float(cantidad_a_sumar)
        request.session['cestaCompra'] = cesta_compra
        request.session['total'] = total
      except:
        mensaje = "Error al añadir al carro"

    #ELIMINAR
    if request.POST.has_key('eliminar') and request.POST.has_key('scroll_box_der'):
      articulo = request.POST['scroll_box_der']
      id_articulo = articulo.split("|")[3]
      precio = articulo.split("|")[2]
      cantidad = articulo.split("|")[1]
      articulo = articulo.split("|")[0]
      for item in cesta_compra:
        if item[0]==articulo:
          precio = item[2]
          tupla = (articulo, cantidad, precio, id_articulo)
          cesta_compra.remove(tupla)
          if item[1]!="1":
            cantidad = str(int(cantidad) - 1)
            tupla = (articulo, cantidad, precio, id_articulo)
            cesta_compra.append(tupla)
          total = total - float(precio)
          break          
      request.session['cestaCompra'] = cesta_compra
      request.session['total'] = total
      
    #CAMBIO
    if request.POST.has_key('calcular') and request.POST.has_key('paga') and request.POST['paga']!="":
      entrega = float(request.POST['paga'])
    if cesta_compra:
      cambio = entrega - total
    else:
      cambio = 0 - entrega
      total = 0
    request.session['paga'] = entrega

    #VENDER
    if request.POST.has_key('vender') and len(cesta_compra)!=0:
      d = datetime.datetime.now()
      minutos = str(d.minute)
      if len(minutos) == 1:
        minutos = "0" + minutos
      fecha = str(d.day) + "/" + str(d.month) + "/" + str(d.year)
      hora = str(d.hour) + ":" + minutos
      importe = str(total)
      if importe.split(".")[1] == 0 and len(importe.split(".")[1]) == 1:
        importe = importe + ".00"
      elif len(importe.split(".")[1]) == 1:
        importe = importe + "0"
      vendedor = Usuario.objects.get(login=request.session['usuario'])
      if cliente_seleccionado != "Sin clasificar":
        comprador = Usuario.objects.get(login=cliente_seleccionado)
        sentenciaVenta = Venta(fecha=fecha, hora=hora, importe=importe, id_vendedor=vendedor, id_comprador=comprador)
        clienteSQL = Usuario.objects.get(login=cliente_seleccionado)
        sentenciaVenta.save()
        sentenciaDeuda = Deuda(id_cliente=clienteSQL, id_venta=sentenciaVenta, pagado="0")
        sentenciaDeuda.save()
        log.write(hora + " - VENTA - " + vendedor.nombre_usuario + " (" + vendedor.login + ") hizo la venta " + str(sentenciaVenta.id) + " a " + comprador.nombre_usuario + " (" + comprador.login + ") con un importe de " + importe + "e\n")
      else:
        sentenciaVenta = Venta(fecha=fecha, hora=hora, importe=importe, id_vendedor=vendedor)
        sentenciaVenta.save()
        log.write(hora + " - VENTA - " + vendedor.nombre_usuario + " (" + vendedor.login + ") hizo la venta " + str(sentenciaVenta.id) + " con un importe de " + importe + "e\n")

      for item in cesta_compra:
        articuloVenta = Articulo.objects.get(nombre_articulo=item[0])
        stock = articuloVenta.stock
        articuloVenta.stock = str(int(stock) - int(item[1]))
        articuloVenta.save()
        i=0
        while i < int(item[1]):
          sentenciaArticuloVenta = Articulo_Venta(id_articulo=articuloVenta, id_venta=sentenciaVenta)
          sentenciaArticuloVenta.save()
          i=i+1

      request.session['listaBusqueda'] = []
      lista_busqueda = []
      request.session['cestaCompra'] = []
      cesta_compra = []
      request.session['total'] = 0.0
      total = 0.0
      request.session['paga'] = 0.0
      paga = 0.0
      request.session['cambio'] = 0.0
      cambio = 0.0
      request.session['cliente_bloqueado'] = "no"
      request.session['cliente_seleccionado'] = "Sin Clasificar"
      cliente_bloqueado = "no"
    tipo_precio = "Normal"

    #LOGOUT
    if request.POST.has_key('Logout'):
      request.session['login']=''
      log.close()
      return render(request, 'login/login.html', "")
    log.close()
    context = {'usuario':request.session['login'],'lista_busqueda':lista_busqueda,'cesta_compra':cesta_compra,'lista_clientes':lista_clientes,'total':total,'cambio':cambio,'entrega':entrega,'cliente_seleccionado':cliente_seleccionado,'cliente_bloqueado':cliente_bloqueado,'mensaje':mensaje}
    return render(request, 'ventas/ventas.html', context)
  else:
    mensaje = ""
    if request.POST.has_key('usuario') and request.POST.has_key('clave'):
      usuario = request.POST['usuario']
      try:
        user = Usuario.objects.get(login=usuario)
        clave_md5 = hashlib.md5(request.POST['clave']).hexdigest()
        for usuarioDB in listaUsuarios:
          if (usuario == usuarioDB.login) and (clave_md5 == usuarioDB.hashpass):
            lista_Usuario_Permiso = Usuario_Permiso.objects.filter(id_usuario=user)
            listaPermisos = []
            for item in lista_Usuario_Permiso:
              permiso = item.id_permiso
              listaPermisos.append(permiso.nombre_permiso)
            request.session['permisos'] = listaPermisos
            request.session['login'] = usuarioDB
            request.session['usuario'] = usuarioDB.login
            usuario_logueado = usuarioDB
            log.close()
            context = {'usuario':usuario_logueado}
            return render(request, 'welcome/welcome.html', context)
          else:
            mensaje = "La clave es incorrecta."
      except:
        mensaje = "El usuario no existe."
    log.close()
    context = {'mensaje':mensaje}
    return render(request, 'login/login.html', context)


def consultarventas(request):
  request.session['articuloCompleto'] = ""
  tiene_permiso = 0
  paginacion = 0
  cuentaVentas = 0
  d = datetime.datetime.now()
  fecha = str(d.day) + "." + str(d.month) + "." + str(d.year)
  log = codecs.open("barra/static/log/log_"+fecha, "a", "utf-8")
  if request.session.has_key('permisos'):
    for nombre_permiso in request.session['permisos']:
      if (nombre_permiso == "Administrador") or (nombre_permiso == "Consultar Ventas"):
        tiene_permiso = 1
  if request.session.has_key('login') and request.session['login']!="" and tiene_permiso == 1:
    lista_consulta = []
    ventas = Venta.objects.all()
    minutos = str(d.minute)
    if len(minutos) == 1:
      minutos = "0" + minutos
    hora = str(d.hour) + ":" + minutos
    log.write(hora + " - CONSULTAR VENTAS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") consulto las ventas.\n")

    for venta in ventas:      
      try:
        articulos_ventas = Articulo_Venta.objects.filter(id_venta=venta)
      except:
        articulos_ventas = []

      if venta.id_comprador == None:
        comprador = ""
      else:
        comprador = venta.id_comprador
      tuplaCabeza = (venta.id, venta.hora, venta.fecha, "", venta.id_vendedor, comprador, venta.importe)
      for item in articulos_ventas:
        articulo = item.id_articulo
        if comprador != "":
          try:
            tipo_precio = venta.id_comprador.id_grupo.tipo_precio
            if tipo_precio == "Normal":
              precio = articulo.precio
            elif tipo_precio == "Reducido":
              precio = articulo.precio2
            elif tipo_precio == "Coste":
              precio = articulo.precio3
          except:
            precio = articulo.precio
        else:
          precio = articulo.precio
        tupla = ("", "", "", articulo.nombre_articulo, "", "", precio)
        lista_consulta.insert(0,tupla)
      lista_consulta.insert(0,tuplaCabeza)

    context = {'usuario':request.session['login'], 'lista_consulta':lista_consulta}
    return render(request, 'consultarVentas/consultarVentas.html', context)
  else:
    return render(request, 'login/login.html', "")

def welcome(request):
  request.session['articuloCompleto'] = ""
  if request.session.has_key('login') and request.session['login']!="":
    context = {'usuario':request.session['login']}
    return render(request, 'welcome/welcome.html', context)
  else:
    return render(request, 'login/login.html', "")


def articulos(request):
  tiene_permiso = 0
  d = datetime.datetime.now()
  minutos = str(d.minute)
  if len(minutos) == 1:
    minutos = "0" + minutos
  hora = str(d.hour) + ":" + minutos
  fecha = str(d.day) + "." + str(d.month) + "." + str(d.year)
  log = codecs.open("barra/static/log/log_"+fecha, "a", "utf-8")
  if request.session.has_key('permisos'):
    for nombre_permiso in request.session['permisos']:
      if (nombre_permiso == "Administrador") or (nombre_permiso == "Articulos"):
        tiene_permiso = 1
  if request.session.has_key('login') and request.session['login']!="" and tiene_permiso == 1:
    mensaje = ""
    codigo_predefinido = ""
    nombre_predefinido = ""
    precio_predefinido = ""
    precio2_predefinido = ""
    precio3_predefinido = ""
    stock_predefinido = 0
    lista_articulos = Articulo.objects.filter(oculto="0")

    try:
      if request.POST.has_key('aniadir'):
        if request.session['articuloCompleto'] != "":
          articuloCompleto = request.session['articuloCompleto']
          request.session['articuloCompleto'] = ""
          articuloCompleto.oculto = "0"
        else:
          articuloCompleto = Articulo()
          articuloCompleto.oculto = "0"
          articuloCompleto.stock = "0"
        if request.POST.has_key('codigo'):
          articuloCompleto.codigo_articulo = request.POST['codigo']
        if request.POST.has_key('nombre'):
          articuloCompleto.nombre_articulo = request.POST['nombre']
        if request.POST.has_key('precio'):
          precio = request.POST['precio']
          if re.search(",", precio):
            trozo = precio.split(",")
            precio = trozo[0] + "." + trozo[1]
          trozo = precio.split(".")
          if len(trozo) == 1:
            precio = precio + ".00"
          elif len(trozo) == 2 and len(trozo[1]) == 1:
            precio = precio + "0"
          articuloCompleto.precio = precio
        if request.POST.has_key('precio2'):
          articuloCompleto.precio2 = request.POST['precio2']
        if request.POST.has_key('precio3'):
          articuloCompleto.precio3 = request.POST['precio3']
        if request.POST.has_key('suma') and request.POST['suma'] != "":
          suma = request.POST['suma']
          articuloCompleto.stock = str(int(str(suma)) + int(articuloCompleto.stock))
        float(articuloCompleto.precio)
        float(articuloCompleto.precio2)
        float(articuloCompleto.precio3)
        articuloCompleto.save()
        log.write(hora + " - ARTICULOS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") aniadio " + str(suma) + " unidades de " + articuloCompleto.nombre_articulo + " con id " + str(articuloCompleto.id) + " con los precios " + articuloCompleto.precio + ", " + articuloCompleto.precio2 + ", " + articuloCompleto.precio3 + "\n")
    except:
      mensaje = "Error al añadir el articulo."

    if request.POST.has_key('cargar') and request.POST.has_key('scroll_box_izq'):
      articuloId = request.POST['scroll_box_izq']
      articuloCompleto = Articulo.objects.get(id=articuloId)
      request.session['articuloCompleto'] = articuloCompleto
      codigo_predefinido = articuloCompleto.codigo_articulo
      nombre_predefinido = articuloCompleto.nombre_articulo
      precio_predefinido = articuloCompleto.precio
      precio2_predefinido = articuloCompleto.precio2
      precio3_predefinido = articuloCompleto.precio3
      stock_predefinido = articuloCompleto.stock

    if request.POST.has_key('eliminar') and request.POST.has_key('scroll_box_izq'):
      articuloId = request.POST['scroll_box_izq']
      articuloCompleto = Articulo.objects.get(id=articuloId)
      articuloCompleto.oculto = "1"
      articuloCompleto.save()
      log.write(hora + " - ARTICULOS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") elimino " + articuloCompleto.nombre_articulo + " con id " + str(articuloCompleto.id) + " con los precios " + articuloCompleto.precio + ", " + articuloCompleto.precio2 + ", " + articuloCompleto.precio3 + "\n")
    elif request.POST.has_key('eliminar') and request.session['articuloCompleto'] != "":
      articuloCompleto = request.session['articuloCompleto']
      request.session['articuloCompleto'] = ""
      articuloCompleto.oculto = "1"
      articuloCompleto.save()
      log.write(hora + " - ARTICULOS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") elimino " + articuloCompleto.nombre_articulo + " con id " + str(articuloCompleto.id) + " con los precios " + articuloCompleto.precio + ", " + articuloCompleto.precio2 + ", " + articuloCompleto.precio3 + "\n")

    log.close()
    context = {'usuario':request.session['login'], 'stock':stock_predefinido, 'lista_articulos':lista_articulos, 'codigo_predefinido':codigo_predefinido, 'nombre_predefinido':nombre_predefinido, 'precio_predefinido':precio_predefinido, 'precio2_predefinido':precio2_predefinido, 'precio3_predefinido':precio3_predefinido, 'mensaje':mensaje}
    return render(request, 'articulos/articulos.html', context)
  else:
    log.close()
    return render(request, 'login/login.html', "")

def ventaspendientes(request):
  request.session['articuloCompleto'] = ""
  tiene_permiso = 0
  d = datetime.datetime.now()
  minutos = str(d.minute)
  if len(minutos) == 1:
    minutos = "0" + minutos
  hora = str(d.hour) + ":" + minutos
  fecha = str(d.day) + "." + str(d.month) + "." + str(d.year)
  log = codecs.open("barra/static/log/log_"+fecha, "a", "utf-8")
  log.write(hora + " - VENTAS PENDIENTES - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") consulto las deudas\n")
  if request.session.has_key('permisos'):
    for nombre_permiso in request.session['permisos']:
      if (nombre_permiso == "Administrador") or (nombre_permiso == "Ventas Pendientes"):
        tiene_permiso = 1
  if request.session.has_key('login') and request.session['login']!="" and tiene_permiso==1:
    numeroDeudas = 0
    morosos = {}
    personas = "personas"
    mensaje = ""

    deudas = Deuda.objects.filter(pagado="0")
    pagos = Pago.objects.all()

    if request.POST.has_key('cobrar') and request.POST.has_key('scroll_box_izq') and request.POST.has_key('paga') and request.POST['paga'] != "":
      try:
        float(request.POST['paga'])
        precio = request.POST['paga']
        if re.search(",", precio):
          trozo = precio.split(",")
          precio = trozo[0] + "." + trozo[1]
        trozo = precio.split(".")
        if len(trozo) == 1:
          precio = precio + ".00"
        elif len(trozo) == 2 and len(trozo[1]) == 1:
          precio = precio + "0"

        d = datetime.datetime.now()
        minutos = str(d.minute)
        if len(minutos) == 1:
          minutos = "0" + minutos
        fecha = str(d.day) + "/" + str(d.month) + "/" + str(d.year)
        hora = str(d.hour) + ":" + minutos

        cobrador = Usuario.objects.get(login=request.session['usuario'])
        cliente = Usuario.objects.get(login=request.POST['scroll_box_izq'])

        sentenciaPaga = Pago(id_cliente=cliente, id_cobrador=cobrador, importe=precio, fecha=fecha, hora=hora)
        sentenciaPaga.save()
        log.write(hora + " - VENTAS PENDIENTES - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") cobro a " + cliente.nombre_usuario + " (" + cliente.login + ") una deuda de " + precio + "e\n")
      except:
        mensaje = "Error al cobrar."

    for deuda in deudas:
      if morosos.has_key(deuda.id_cliente.id):
        tupla = morosos[deuda.id_cliente.id]
        sumando1 = float(tupla[1])
        sumando2 = float(deuda.id_venta.importe)
        suma = str(sumando1 + sumando2)

        trozo = suma.split(".")
        if len(trozo) == 1:
          suma = suma + ".00"
        elif len(trozo) == 2 and len(trozo[1]) == 1:
          suma = suma + "0"
        
        morosos[deuda.id_cliente.id] = (deuda.id_cliente, suma)
      else:
        morosos[deuda.id_cliente.id] = (deuda.id_cliente, deuda.id_venta.importe)

    for pago in pagos:
      resta = str(float(morosos[pago.id_cliente.id][1]) - float(pago.importe))
      if resta != "0.0":
        morosos[pago.id_cliente.id] = (morosos[pago.id_cliente.id][0], resta)
      else:
        del morosos[pago.id_cliente.id]

    numeroDeudas = len(morosos)

    if numeroDeudas == 1:
      personas = "persona"
    log.close()
    context = {'usuario':request.session['login'], 'numeroDeudas':numeroDeudas, 'personas':personas, 'morosos':morosos, 'mensaje':mensaje}
    return render(request, 'ventasPendientes/ventasPendientes.html', context)
  else:
    log.close()
    return render(request, 'login/login.html', "")

def notas(request):
  request.session['articuloCompleto'] = ""
  tiene_permiso = 0
  d = datetime.datetime.now()
  fecha = str(d.day) + "." + str(d.month) + "." + str(d.year)
  log = codecs.open("barra/static/log/log_"+fecha, "a", "utf-8")
  if request.session.has_key('permisos'):
    for nombre_permiso in request.session['permisos']:
      if (nombre_permiso == "Administrador") or (nombre_permiso == "Notas"):
        tiene_permiso = 1
  if request.session.has_key('login') and request.session['login']!="" and tiene_permiso==1:
    log.close()
    context = {'usuario':request.session['login']}
    return render(request, 'notas/notas.html', context)
  else:
    log.close()
    return render(request, 'login/login.html', "")

def usuario(request):
  request.session['articuloCompleto'] = ""
  d = datetime.datetime.now()
  minutos = str(d.minute)
  if len(minutos) == 1:
    minutos = "0" + minutos
  hora = str(d.hour) + ":" + minutos
  fecha = str(d.day) + "." + str(d.month) + "." + str(d.year)
  log = codecs.open("barra/static/log/log_"+fecha, "a", "utf-8")
  if request.session.has_key('login') and request.session['login']!="":
    cliente = Usuario.objects.get(login=request.session['usuario'])
    deudas = Deuda.objects.filter(id_cliente=cliente)    
    ventas = Venta.objects.filter(id_comprador=cliente)
    pagos = Pago.objects.filter(id_cliente=cliente)
    
    importe = 0
    importe2 = 0
    suma = 0
    suma2 = 0
    suma3 = 0
    lista_consulta = []

    try:
      grupo_usuario = cliente.id_grupo.nombre_grupo
    except:
      grupo_usuario = "Sin grupo"

    for venta in ventas:
      importe = importe + float(venta.importe)
      try:
        articulos_ventas = Articulo_Venta.objects.filter(id_venta=venta)
      except:
        articulos_ventas = []
      tupla = (venta.hora, venta.fecha, "", venta.id_vendedor.nombre_usuario, venta.importe)
      lista_consulta.append(tupla)
      for item in articulos_ventas:
        articulo = item.id_articulo
        tipo_precio = venta.id_comprador.id_grupo.tipo_precio
        if tipo_precio == "Normal":
          precio = item.id_articulo.precio
        elif tipo_precio == "Reducido":
          precio = item.id_articulo.precio2
        elif tipo_precio == "Coste":
          precio = item.id_articulo.precio3
        tupla = ("", "", item.id_articulo.nombre_articulo, "", precio)
        lista_consulta.append(tupla)

    for pago in pagos:
      importe2 = importe2 + float(pago.importe)

    suma = str(importe)
    trozo = suma.split(".")
    if len(trozo) == 1:
      suma = suma + ".00"
    elif len(trozo) == 2 and len(trozo[1]) == 1:
      suma = suma + "0"

    suma2 = str(importe2)
    trozo2 = suma2.split(".")
    if len(trozo2) == 1:
      suma2 = suma2 + ".00"
    elif len(trozo2) == 2 and len(trozo2[1]) == 1:
      suma2 = suma2 + "0"

    suma3 = str(float(suma) - float(suma2))
    trozo3 = suma3.split(".")
    if len(trozo3) == 1:
      suma3 = suma3 + ".00"
    elif len(trozo3) == 2 and len(trozo3[1]) == 1:
      suma3 = suma3 + "0"

    if request.POST.has_key('clavevieja') and request.POST.has_key('clavenueva') and request.POST.has_key('clavenueva2') and request.POST.has_key('clave') and (request.POST['clavenueva'] == request.POST['clavenueva2']):
      md5 = hashlib.md5(request.POST['clavevieja']).hexdigest()
      if md5 == cliente.hashpass:
        nuevo_md5 = hashlib.md5(request.POST['clavenueva']).hexdigest()
        cliente.hashpass = nuevo_md5
        cliente.save()
        log.write(hora + " - USUARIO - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") cambio su clave\n")

    log.close()
    context = {'usuario':cliente.nombre_usuario,'deudas':deudas, 'login':cliente.login, 'grupo':grupo_usuario, 'gastado':suma, 'pagado':suma2, 'deuda':suma3, 'lista_consulta':lista_consulta}
    return render(request, 'usuario/usuario.html', context)
  else:
    log.close()
    return render(request, 'login/login.html', "")

def gestionUsuarios(request):
  request.session['articuloCompleto'] = ""
  tiene_permiso = 0
  d = datetime.datetime.now()
  minutos = str(d.minute)
  if len(minutos) == 1:
    minutos = "0" + minutos
  hora = str(d.hour) + ":" + minutos
  fecha = str(d.day) + "." + str(d.month) + "." + str(d.year)
  log = codecs.open("barra/static/log/log_"+fecha, "a", "utf-8")
  if request.session.has_key('permisos'):
    for nombre_permiso in request.session['permisos']:
      if (nombre_permiso == "Administrador") or (nombre_permiso == "Gestion Usuarios"):
        tiene_permiso = 1
  if request.session.has_key('login') and request.session['login']!="" and tiene_permiso==1:
    lista_grupos = []
    lista_usuarios = []
    ya_existe = 0

    nombre_usuario = ""
    dni = ""
    nickname = ""
    grupo_usuario = ""
    lista_grupos.append("Ninguno")

    grupos = Grupo.objects.all()
    for grupo in grupos:
      lista_grupos.append(grupo.nombre_grupo)

    usuarios = Usuario.objects.all()
    for user in usuarios:
      tupla = (user.dni, user.nombre_usuario, user.login)
      lista_usuarios.append(tupla)

    if request.POST.has_key('scroll_box_izq') and request.POST.has_key('escogerUsuario'):
      nickname = request.POST['scroll_box_izq']
      for user in usuarios:
        if nickname == user.login:
          dni = user.dni
          nombre_usuario = user.nombre_usuario
          try:
            grupo_usuario = user.id_grupo.nombre_grupo
          except:
            grupo_usuario = "Ninguno"
          log.write(hora + " - GESTION USUARIOS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") consulto el usuario " + user.nombre_usuario + " (" + user.login + ")\n")

    if request.POST.has_key('scroll_box_izq') and request.POST.has_key('eliminarUsuario'):
      user = Usuario.objects.get(login=request.POST['scroll_box_izq'])
      log.write(hora + " - GESTION USUARIOS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") elimino el usuario " + user.nombre_usuario + " (" + user.login + ")\n")
      user.delete()

    if request.session.has_key('usuario_seleccionado') and request.session['usuario_seleccionado']!="":
      usuario_seleccionado = request.session['usuario_seleccionado']

    if request.POST.has_key('aniadirUsuario') and request.POST.has_key('nombre_usuario') and request.POST.has_key('nickname') and request.POST.has_key('dni') and request.POST['nombre_usuario']!="" and request.POST['nickname']!="" and request.POST['dni']!="":
      for user in usuarios:
        if user.login == request.POST['nickname']:
          ya_existe = 1
      if ya_existe != 1:
        if request.POST.has_key('clave1') and request.POST.has_key('clave1') and request.POST['clave1']!="" and request.POST['clave2']==request.POST['clave2']:
          clave_md5 = hashlib.md5(request.POST['clave1']).hexdigest()
          if request.POST['grupo_box'] != "Ninguno":
            sentencia = Usuario(nombre_usuario=request.POST['nombre_usuario'],dni=request.POST['dni'],login=request.POST['nickname'],hashpass=clave_md5,id_grupo=Grupo.objects.get(nombre_grupo=request.POST['grupo_box']))
          else:
            sentencia = Usuario(nombre_usuario=request.POST['nombre_usuario'],dni=request.POST['dni'],login=request.POST['nickname'],hashpass=clave_md5)
          sentencia.save()
          log.write(hora + " - GESTION USUARIOS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") aniadio el usuario " + request.POST['nombre_usuario'] + " (" + request.POST['nickname'] + ")\n")
      else:
        user = Usuario.objects.get(login=request.POST['nickname'])
        user.nombre_usuario = request.POST['nombre_usuario']
        user.dni = request.POST['dni']
        user.id_grupo = Grupo.objects.get(nombre_grupo=request.POST['grupo_box'])
        user.save()
        log.write(hora + " - GESTION USUARIOS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") modifico el usuario " + user.nombre_usuario + " (" + user.login + ")\n")
      ya_existe = 0
    usuarios = Usuario.objects.all()
    lista_usuarios = []
    for user in usuarios:
      tupla = (user.dni, user.nombre_usuario, user.login)
      lista_usuarios.append(tupla)
    log.close()
    context = {'usuario':request.session['login'],'lista_usuarios':lista_usuarios,'nombre_usuario':nombre_usuario,'dni':dni,'nickname':nickname,'lista_grupos':lista_grupos,'grupo_usuario':grupo_usuario}
    return render(request, 'gestionUsuarios/gestionUsuarios.html', context)
  else:
    log.close()
    return render(request, 'login/login.html', "")

def gestionGrupos(request):
  request.session['articuloCompleto'] = ""
  tiene_permiso = 0
  d = datetime.datetime.now()
  minutos = str(d.minute)
  if len(minutos) == 1:
    minutos = "0" + minutos
  hora = str(d.hour) + ":" + minutos
  fecha = str(d.day) + "." + str(d.month) + "." + str(d.year)
  log = codecs.open("barra/static/log/log_"+fecha, "a", "utf-8")
  if request.session.has_key('permisos'):
    for nombre_permiso in request.session['permisos']:
      if (nombre_permiso == "Administrador") or (nombre_permiso == "Gestion Grupos"):
        tiene_permiso = 1
  if request.session.has_key('login') and request.session['login']!="" and tiene_permiso==1:
    lista_tipo_precio = ["Normal","Reducido","Coste"]
    lista_grupos = []
    grupo_seleccionado = ""
    tipo_precio_grupo = ""

    grupos = Grupo.objects.all()
    for grupo in grupos:
      tupla = (grupo.nombre_grupo, grupo.tipo_precio)
      lista_grupos.append(tupla)

    if request.POST.has_key('campo_crear_grupo') and request.POST.has_key('crear_grupo') and request.POST['campo_crear_grupo']!="":
      sentencia = Grupo(nombre_grupo=request.POST['campo_crear_grupo'],tipo_precio="")
      sentencia.save()
      log.write(hora + " - GESTION GRUPOS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") creo el grupo " + sentencia.nombre_grupo + "\n")

    if request.POST.has_key('scroll_box_izq') and request.POST.has_key('escogerGrupo'):
      string = request.POST['scroll_box_izq']
      trozo = string.split("|")
      grupo_seleccionado = trozo[0]
      tipo_precio_grupo = trozo[1]
      request.session['grupo_seleccionado'] = grupo_seleccionado
      log.write(hora + " - GESTION GRUPOS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") consulto el grupo " + grupo_seleccionado + "\n")

    if request.POST.has_key('scroll_box_der') and request.POST.has_key('aniadirTipo') and request.session['grupo_seleccionado']!="":
      for grupo in grupos:
        if grupo.nombre_grupo==request.session['grupo_seleccionado']:
          grupo.tipo_precio = request.POST['scroll_box_der']
          grupo.save()
          log.write(hora + " - GESTION GRUPOS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") le dio el tipo de precio " + grupo.tipo_precio + " al grupo " + grupo.nombre_grupo + "\n")
          grupos = Grupo.objects.all()
          lista_grupos=[]
          for grupo in grupos:
            tupla = (grupo.nombre_grupo, grupo.tipo_precio)
            lista_grupos.append(tupla)
      request.session['grupo_seleccionado']=""

    grupos = Grupo.objects.all()
    lista_grupos = []
    for grupo in grupos:
      tupla = (grupo.nombre_grupo, grupo.tipo_precio)
      lista_grupos.append(tupla)

    log.close()
    context = {'usuario':request.session['login'],'lista_tipo_precio':lista_tipo_precio,'lista_grupos':lista_grupos,'grupo_seleccionado':grupo_seleccionado,'tipo_precio_grupo':tipo_precio_grupo}
    return render(request, 'gestionGrupos/gestionGrupos.html', context)
  else:
    log.close()
    return render(request, 'login/login.html', "")

def permisos(request):
  request.session['articuloCompleto'] = ""
  tiene_permiso = 0
  d = datetime.datetime.now()
  minutos = str(d.minute)
  if len(minutos) == 1:
    minutos = "0" + minutos
  hora = str(d.hour) + ":" + minutos
  fecha = str(d.day) + "." + str(d.month) + "." + str(d.year)
  log = codecs.open("barra/static/log/log_"+fecha, "a", "utf-8")
  if request.session.has_key('permisos'):
    for nombre_permiso in request.session['permisos']:
      if (nombre_permiso == "Administrador") or (nombre_permiso == "Gestion Permisos"):
        tiene_permiso = 1
  if request.session.has_key('login') and request.session['login']!="" and tiene_permiso==1:
    lista_usuarios = []
    lista_permisos = []
    lista_permisos_usuario = []
    usuario_seleccionado = ""
    usuarios = Usuario.objects.all()
    permisos = Permiso.objects.all()    

    for user in usuarios:
      tupla = (user.dni, user.nombre_usuario, user.login)
      lista_usuarios.append(tupla)
    if request.POST.has_key('scroll_box_izq') and request.POST.has_key('escogerUsuario'):
      usuario_seleccionado = request.POST['scroll_box_izq']
      request.session['usuario_seleccionado'] = request.POST['scroll_box_izq']
      user = Usuario.objects.get(login=request.POST['scroll_box_izq'])
      log.write(hora + " - GESTION PERMISOS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") consulto el usuario " + user.nombre_usuario + " (" + user.login + ")\n")

    if request.session.has_key('usuario_seleccionado') and request.session['usuario_seleccionado']!="":
      usuario_seleccionado = request.session['usuario_seleccionado']

    for permiso in permisos:
      lista_permisos.append(permiso.nombre_permiso)

    if usuario_seleccionado!="":
      usuario = Usuario.objects.get(login=usuario_seleccionado)
      permisos_usuario = Usuario_Permiso.objects.filter(id_usuario=usuario)
      for item in permisos_usuario:
        permiso = item.id_permiso
        lista_permisos_usuario.append(permiso.nombre_permiso)

    if usuario_seleccionado!="" and request.POST.has_key('scroll_box_der') and request.POST['scroll_box_der']!="" and request.POST.has_key('borrarPermiso'):
      for item in permisos_usuario:
        permiso = item.id_permiso
        if permiso.nombre_permiso == request.POST['scroll_box_der']:
          log.write(hora + " - GESTION PERMISOS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") elimino el permiso " + permiso.nombre_permiso + " del usuario " + usuario.nombre_usuario + " (" + usuario.login + ")\n")
          item.delete()
      request.session['usuario_seleccionado'] = ""
          
    if usuario_seleccionado!="" and request.POST.has_key('scroll_box_der') and request.POST['scroll_box_der']!="" and request.POST.has_key('aniadirPermiso'):
      permiso = Permiso.objects.get(nombre_permiso=request.POST['scroll_box_der'])
      sentencia = Usuario_Permiso(id_usuario=usuario, id_permiso=permiso)
      sentencia.save()
      request.session['usuario_seleccionado'] = ""
      log.write(hora + " - GESTION PERMISOS - " + request.session['login'].nombre_usuario + " (" + request.session['login'].login + ") aniadio el permiso " + permiso.nombre_permiso + " al usuario " + usuario.nombre_usuario + " (" + usuario.login + ")\n")

    if usuario_seleccionado!="":
      usuario = Usuario.objects.get(login=usuario_seleccionado)
      permisos_usuario = Usuario_Permiso.objects.filter(id_usuario=usuario)
      lista_permisos_usuario = []
      for item in permisos_usuario:
        permiso = item.id_permiso
        lista_permisos_usuario.append(permiso.nombre_permiso)

    log.close()
    context = {'usuario':request.session['login'],'lista_usuarios':lista_usuarios,'usuario_seleccionado':usuario_seleccionado,'lista_permisos':lista_permisos,'lista_permisos_usuario':lista_permisos_usuario}
    return render(request, 'permisos/permisos.html', context)
  else:
    log.close()
    return render(request, 'login/login.html', "")

def administracion(request):
  request.session['articuloCompleto'] = ""
  tiene_permiso = 0
  d = datetime.datetime.now()
  fecha = str(d.day) + "." + str(d.month) + "." + str(d.year)
  log = codecs.open("barra/static/log/log_"+fecha, "a", "utf-8")
  if request.session.has_key('permisos'):
    for nombre_permiso in request.session['permisos']:
      if (nombre_permiso == "Administrador") or (nombre_permiso == "Gestion Administracion"):
        tiene_permiso = 1
  if request.session.has_key('login') and request.session['login']!="" and tiene_permiso==1:
    log.close()
    context = {'usuario':request.session['login']}
    return render(request, 'administracion/administracion.html', context)
  else:
    log.close()
    return render(request, 'login/login.html', "")

def facturacion(request):
  request.session['articuloCompleto'] = ""
  tiene_permiso = 0
  d = datetime.datetime.now()
  fecha = str(d.day) + "." + str(d.month) + "." + str(d.year)
  log = codecs.open("barra/static/log/log_"+fecha, "a", "utf-8")
  if request.session.has_key('permisos'):
    for nombre_permiso in request.session['permisos']:
      if (nombre_permiso == "Administrador") or (nombre_permiso == "Gestion Administracion"):
        tiene_permiso = 1
  if request.session.has_key('login') and request.session['login']!="" and tiene_permiso==1:

    ganado = 0
    ventasTotales = Venta.objects.all()
    for venta in ventasTotales:
      ganado = ganado + float(venta.importe)
      
    log.close()
    context = {'usuario':request.session['login'],'importe':ganado}
    return render(request, 'facturacion/facturacion.html', context)
  else:
    log.close()
    return render(request, 'login/login.html', "")