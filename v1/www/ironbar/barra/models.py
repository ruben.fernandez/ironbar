from django.db import models

# Create your models here.

class Permiso(models.Model):
  nombre_permiso = models.CharField(max_length=50)

  def __unicode__ (self):
    return self.nombre_permiso

class Grupo(models.Model):
  nombre_grupo = models.CharField(max_length=200)
  tipo_precio = models.CharField(max_length=10)

  def __unicode__ (self):
    return self.nombre_grupo

class Usuario(models.Model):
  nombre_usuario = models.CharField(max_length=200)
  dni = models.CharField(max_length=10)
  login = models.CharField(max_length=100, unique=True)
  hashpass = models.CharField(max_length=1000)
  #is_logeado = models.CharField(max_length=5)
  id_grupo = models.ForeignKey(Grupo, null=True)

  def __unicode__ (self):
    return self.nombre_usuario

class Usuario_Permiso(models.Model):
  id_usuario = models.ForeignKey(Usuario)
  id_permiso = models.ForeignKey(Permiso)

class Venta(models.Model):
  fecha = models.CharField(max_length=10)
  hora = models.CharField(max_length=5)
  importe = models.CharField(max_length=50)
  id_vendedor = models.ForeignKey(Usuario, related_name='vendedor')
  id_comprador = models.ForeignKey(Usuario, related_name='comprador', null=True)

  def __unicode__ (self):
    return str(self.fecha) + " " + str(self.hora)

class Articulo(models.Model):
  codigo_articulo = models.CharField(max_length=100)
  nombre_articulo = models.CharField(max_length=200)
  precio = models.CharField(max_length=50)
  precio2 = models.CharField(max_length=50)
  precio3 = models.CharField(max_length=50)
  stock = models.CharField(max_length=50)
  oculto = models.CharField(max_length=2, default='0')

  def __unicode__ (self):
    return self.nombre_articulo

class Articulo_Venta(models.Model):
  id_articulo = models.ForeignKey(Articulo)
  id_venta = models.ForeignKey(Venta)

 # def __unicode__ (self):
  #  return str(self.id_articulo) + " " + str(self.id_venta)

class Deuda(models.Model):
  id_cliente = models.ForeignKey(Usuario)
  id_venta = models.ForeignKey(Venta)
  pagado = models.CharField(max_length=2)

class Pago(models.Model):
  id_cliente = models.ForeignKey(Usuario, related_name="cliente")
  id_cobrador = models.ForeignKey(Usuario, related_name="cobrador")
  importe = models.CharField(max_length=50)
  fecha = models.CharField(max_length=10)
  hora = models.CharField(max_length=5)
