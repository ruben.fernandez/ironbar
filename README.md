# IRONBAR V2 #

### Resume ###
This is a program made by Rubén Fernández Lago for Freak's Party, ​a cultural association at University of A Coruña during the Degree Project. If you need assistance on the application please contact me at leegaara@gmail.com

### Project ###
hydra.des.udc.es/proyecto.pdf

### Required ###
* django 1.6.5
* MySQL 5.5
* python 2.7
* python-mysqldb 1.2.3-2
* python-setuptools 0.6.24-1
* pyttsx 1.1

### Install ###

```
#!bash
apt-get install mysql-server
apt-get install python
apt-get install python-mysqldb
apt-get install python-setuptools
easy_install django
easy_install pygooglechart
easy_install pyttsx
```

### Configure data base ###
```
#!sql
mysql -u root -p
> CREATE DATABASE barra;
> GRANT all ON barra.* TO 'ironbar'@'localhost' IDENTIFIED BY 'ironbar';
> exit;
```

### Configure project ###
```
#!bash
python manage.py syncdb
```

### Launch project ###

```
#!bash

python manage.py runserver
```

### Author Rubén Fernández Lago ###
* [LinkedIn](https://www.linkedin.com/profile/view?id=201017251&trk=nav_responsive_tab_profile)
* [Google Plus](https://plus.google.com/+Rub%C3%A9nFern%C3%A1ndezLago)
