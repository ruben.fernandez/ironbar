# -*- coding: utf-8 -*-
from django.contrib.auth import logout as auth_logout
from django.shortcuts import render, redirect
from django.http import HttpResponse
from email.mime.text import MIMEText
from pygooglechart import PieChart3D
from barra.models import *

import unicodedata
import datetime
import smtplib
import random
import json
import sys

class req_vars:
    def __init__(self, request):
        self.r = request

    def get(self, var):
    	if self.r.POST.has_key(var):
        	return self.r.POST.get(var)
        elif self.r.GET.has_key(var):
        	return self.r.GET.get(var)
        else:
        	return None

class my_session:
	def __init__(self, session):
		self.session = session

	def get(self, var):
		return self.session.get(var)

	def get_if_exist(self, var, vacio):
		if self.is_exist(var):
			return self.session.get(var)
		else:
			return vacio

	def is_exist(self, var):
		return self.session.has_key(var)

	def put(self, name, var):
		self.session[name] = var

	def delete(self, name):
		del self.session[name]

class DateTime_Controller:
	delta = "finish"
	day = "01"
	month = "01"
	year = "3000"
	hour = "00"
	minute = "00"
	second = "00"	
	nulo = False

	def __init__(self, delta):
		if delta == "init":
			self.year = "2000"
			self.delta = delta # Sirve para saber si es una fecha inicial o final.


	def is_nulo(self):
		return self.nulo

	def set_from_datepicker(self, date, time):
		self.init = True
		if time!="":
			self.nulo = False
			s = time.split(":")
			self.hour = s[0]
			self.minute = s[1]

			if len(self.hour)==1:
				self.hour = "0" + self.hour

			if len(self.minute)==1:
				self.minute = "0" + self.minute

			if len(self.second)==1:
				self.second = "0" + self.second
		else:
			self.nulo = True
			self._init_time()

		if date!="":
			self.nulo = False
			s = date.split(" ")
			self.day = s[0]
			self.year = s[2]
			month = s[1][:-1]

			if month == "January":
				self.month = "01"
			elif month == "February":
				self.month = "02"
			elif month == "March":
				self.month = "03"
			elif month == "April":
				self.month = "04"
			elif month == "May":
				self.month = "05"
			elif month == "June":
				self.month = "06"
			elif month == "July":
				self.month = "07"
			elif month == "August":
				self.month = "08"
			elif month == "September":
				self.month = "09"
			elif month == "October":
				self.month = "10"
			elif month == "November":
				self.month = "11"
			else:
				self.month = "12"

			if len(self.day)==1:
				self.day = "0" + self.day
		else:
			self.nulo = True
			self._init_date()

	def get_to_djangodb(self):
		# 2014-04-20 16:07:08+00:00
		return self.year + "-" + self.month + "-" + self.day + " " + self.hour + ":" + self.minute + ":" + self.second + "+00:00"

	def get_to_djangodb_datetime(self):
		return datetime.datetime(int(self.year), int(self.month), int(self.day), int(self.hour), int(self.minute), int(self.second))

	def set_from_djangodb(self, datetime):
		self.init = True
		d = datetime.split(" ")
		fecha = d[0].split("-")
		hora = d[1].split(":")

		self.day = fecha[2]
		self.month = fecha[1]
		self.year = fecha[0]
		self.hour = hora[0]
		self.minute = hora[1]
		self.second = hora[2][0:1]

	def _init_time(self):
		self.hour = "00"
		self.minute = "00"
		self.second = "00"

	def _init_date(self):
		self.day = "01"
		self.month = "01"
		if self.delta == "init":
			self.year = "1111"
		else:
			self.year = "9999"


class Cart:
	cesta = [] # (producto, precio, cantidad)
	tipo_precio_efectivo = None

	def __init__(self, tipo_precio_efectivo):
		self.tipo_precio_efectivo = tipo_precio_efectivo
		cesta = []

	def load_from_session(self, cookie_session):
		session = my_session(cookie_session)
		if session.is_exist('cesta'):
			cesta_json = json.loads( session.get('cesta') )
			for linea in cesta_json:
				producto = Producto.objects.get(codigo_de_barras=linea[0]['codigo_de_barras'])
				precio = Precio.objects.get(producto=producto, tipo_precio=self.tipo_precio_efectivo)
				cantidad = int(linea[2])
				self.cesta.append((producto, precio, cantidad))

	def load(self, lista_cesta):
		self.cesta = []
		self.cesta = lista_cesta

	def add(self, producto, precio, cantidad):
		linea_nueva = (producto, precio, cantidad)
		encontrado = False
		if self.cesta:
			for linea in self.cesta:
				if producto == linea[0]:
					encontrado = True
					linea_nueva = (producto, precio, linea[2] + 1)
					self.cesta[self.cesta.index(linea)] = linea_nueva
			if not encontrado:
				self.cesta.append(linea_nueva)
		else:
			self.cesta.append(linea_nueva)

	def remove(self, codigo_de_barras):
		for fila in self.cesta:
			if fila[0].codigo_de_barras == codigo_de_barras:
				self.cesta.remove(fila)

	def refresh(self):
		for linea in self.cesta:
			precio = Precio.objects.get(producto=linea[0], tipo_precio=self.tipo_precio_efectivo)
			linea_nueva = (linea[0], precio, linea[2])
			self.cesta[self.cesta.index(linea)] = linea_nueva

	def set_tipo_precio_efectivo(self, tipo_precio_efectivo):
		self.tipo_precio_efectivo = tipo_precio_efectivo

	def get_tipo_precio_efectivo(self):
		return self.tipo_precio_efectivo

	def get(self):
		return self.cesta

	def total(self):
		importe_total = 0
		for linea in self.cesta:
			importe_total = importe_total + linea[1].precio*linea[2]
		return importe_total

	def clear(self):
		self.cesta = []

def do_login(request, html, context, required_permissions):
	if request.user.is_authenticated():
		if check_permissions(request.user.email, required_permissions):
			if request.GET.has_key('logout_button'):
				auth_logout(request)
				if html=='index.html':
					return redirect('/')
				else:
					return redirect(html.split('.')[0])
			context['usuario_gastado'] = calcular_gastado(request.user)
			context['pedidos_pendientes'] = Factura.objects.filter(pedido_entregado=False).count()
			return render(request, html, context)
		else:
			return render(request, 'no_permission.html', {})
	else:
		return redirect('/login')

def check_permissions(user_email, required_permissions):
	if not required_permissions:
		return True
	user = Usuario.objects.get(email=user_email)
	permission_user_list = Permiso_Usuario.objects.filter(usuario=user)
	for permission_user in permission_user_list:
		if permission_user.permiso.nombre in required_permissions:
			return True
	permission_group_list = Permiso_Grupo.objects.filter(grupo=user.grupo)
	for permission_group in permission_group_list:
		if permission_group.permiso.nombre in required_permissions:
			return True
	if user.is_staff:
		return True
	return False

def check_request(request, elements):
	for item in elements:
		if request.POST.has_key(item) and request.POST[item]=="":
			return False
	return True

def check_submit(request, var):
	if request.POST.has_key(var) or request.GET.has_key(var):
		return True
	else:
		return False

def check_get_submit(request, var):
	return request.GET.has_key(var)

def error(msg):
	return HttpResponse(msg)

def send_mail(to, subject, msg):
	username = 'ironbarv2'
	password = 'susohuelemil'

	msg = MIMEText(msg.encode('utf-8'), 'plain', 'utf-8')
	msg['From'] 	= 'ironbarv2@gmail.com'
	msg['To']	= to
	msg['Subject']	= subject

	s = smtplib.SMTP('smtp.gmail.com:587')
	s.starttls()
	s.ehlo()
	s.login(username,password)
	s.sendmail(msg['From'], msg['To'], msg.as_string())
	s.quit()

def generate_barcode():
	barcode = random.randrange(10000000, 99999999, 1)
	try:
		products = Producto.objects.get(codigo_de_barras=barcode)
		return generate_barcode()
	except:
		return barcode

def ip_server():
	direc = sys.argv[-1]
	s = direc.split[":"]
	print s[0]
	return s[0]

def port_server():
	s = sys.argv[-1].split[":"]
	print s[1]
	return s[1]

def serializar_cesta(lista):
	resultado = []
	for item in lista:
		resultado.append((item[0].id,item[1].id,item[2]))
	return resultado

def deserializar_cesta(texto):
	lista = []
	for item in texto:
		producto = Producto.objects.get(id=item[0])
		precio = Precio.objects.get(id=item[1])
		lista.append((producto,precio,item[2]))
	return lista

def calcular_gastado(usuario):
	gastado = 0
	for factura in Factura.objects.filter(cliente=usuario, pagada=False):
		gastado = gastado + factura.importe
	return gastado

def load_from_configuration_file(variable):
	valor = ""
	fichero = open("barra/config.cfg","r")
	for linea in fichero:
		final = linea.rfind("]")
		if final != -1:
			if variable == linea[1:final]:
				valor = linea.strip("["+variable+"]=")
				break
	fichero.close()
	return valor[:-1]

def generarGraficaTarta(x, y, labels, datos, nombreFichero):
	chart = PieChart3D(x, y)
	chart.set_pie_labels(labels)
	chart.add_data(datos)
	chart.download("barra/static/images/charts/" + nombreFichero + ".png")
	
def elimina_tildes(cadena):
    s = ''.join((c for c in unicodedata.normalize('NFD',unicode(cadena)) if unicodedata.category(c) != 'Mn'))
    return s.decode()