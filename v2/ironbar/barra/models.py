from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin, BaseUserManager

# Create your models here.

class Producto(models.Model):
	codigo_de_barras = models.CharField(max_length=200, unique=True, db_index=True)
	nombre = models.CharField(unique=True, max_length=200, db_index=True)
	stock = models.IntegerField()

	def __unicode__(self):
		return self.nombre

class Tipo_Precio(models.Model):
	nombre = models.CharField(max_length=200, unique=True)
	default = models.BooleanField(default=False)
	cost = models.BooleanField(default=False)
	descripcion = models.CharField(max_length=200)

	def __unicode__(self):
		return self.nombre
		
class Precio(models.Model):
	producto = models.ForeignKey(Producto)
	tipo_precio = models.ForeignKey(Tipo_Precio)
	precio = models.FloatField()
	
class Grupo(models.Model):
	nombre = models.CharField(max_length=200, unique=True)
	descripcion = models.CharField(max_length=200)
	tipo_precio = models.ForeignKey(Tipo_Precio)

	def __unicode__(self):
		return self.nombre

class UsuarioManager(BaseUserManager):
	def _create_user(self, email, password, dni, is_superuser, is_staff, nombre, apellidos):
		if not email:
			raise ValueError('El email es obligatorio.')
		email = self.normalize_email(email) #pasa a minusculas
		usuario = self.model(email=email, dni=dni, is_superuser=is_superuser, is_staff=is_staff, nombre=nombre, apellidos=apellidos)
		usuario.set_password(password)
		usuario.save(using=self._db)
		return usuario

	def create_user(self, email, password, dni, nombre, apellidos):
		return self._create_user(email, password, dni, False, False, nombre, apellidos)

	def create_superuser(self, email, password, dni, nombre, apellidos):
		return self._create_user(email, password, dni, True, True, nombre, apellidos)

class Usuario(AbstractBaseUser, PermissionsMixin):
	nombre = models.CharField(max_length=200)
	apellidos = models.CharField(max_length=200)
	dni = models.CharField(max_length=10, unique=True)
	email = models.CharField(max_length=100, unique=True)
	grupo = models.ForeignKey(Grupo, null=True)
	codigo_validacion = models.CharField(max_length=40, null=True)

	objects = UsuarioManager()

	is_active = models.BooleanField(default=False)
	is_staff = models.BooleanField(default=False)

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = ['nombre','apellidos','dni']

	def get_full_name(self):
		return self.nombre + " " + self.apellidos

	def get_short_name(self):
		return self.nombre

	def __unicode__(self):
		return self.nombre + " " + self.apellidos

class Permiso(models.Model):
	nombre = models.CharField(max_length=200, unique=True)
	descripcion = models.CharField(max_length=200)

	def __unicode__(self):
		return self.nombre

class Factura(models.Model):
	fecha = models.DateTimeField()
	importe = models.FloatField()
	cliente = models.ForeignKey(Usuario, related_name='cliente', null=True)
	vendedor = models.ForeignKey(Usuario, related_name='vendedor')
	pagada = models.BooleanField(default=False)
	pedido_entregado = models.BooleanField(default=False)

	def __unicode__(self):
		return "Factura " + str(self.id)

class Linea(models.Model):
	producto_codigo_de_barras = models.CharField(max_length=200)
	producto_nombre = models.CharField(max_length=200)
	producto_cantidad_vendida = models.IntegerField()
	producto_precio = models.FloatField()
	factura = models.ForeignKey(Factura)

	def importe_total(self):
		r = self.producto_cantidad_vendida*self.producto_precio
		return r

	def __unicode__(self):
		return "Linea " + str(self.id) + " de la factura " + str(self.factura.id)

class Permiso_Grupo(models.Model):
	permiso = models.ForeignKey(Permiso)
	grupo = models.ForeignKey(Grupo)

	def __unicode__(self):
		return self.grupo.nombre + " - " + self.permiso.nombre

class Permiso_Usuario(models.Model):
	usuario = models.ForeignKey(Usuario)
	permiso = models.ForeignKey(Permiso)

	def __unicode__(self):
		return self.usuario.nombre + " - " + self.permiso.nombre

class Evento(models.Model):
	nombre = models.CharField(max_length=200, unique=True)
	fecha_inicio = models.DateTimeField()
	fecha_final = models.DateTimeField()