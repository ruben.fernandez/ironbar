INSERT INTO barra_permiso (nombre, descripcion) VALUES ('Sell', 'Access to Sell page');
INSERT INTO barra_permiso (nombre, descripcion) VALUES ('Sales', 'Access to Sales page');
INSERT INTO barra_permiso (nombre, descripcion) VALUES ('Orders', 'Access to Orders page');
INSERT INTO barra_permiso (nombre, descripcion) VALUES ('Products', 'Access to Products page');
INSERT INTO barra_permiso (nombre, descripcion) VALUES ('Liabilities', 'Access to Liabilities page');
INSERT INTO barra_permiso (nombre, descripcion) VALUES ('Billing', 'Access to Billing page');
INSERT INTO barra_permiso (nombre, descripcion) VALUES ('Administration', 'Access to Admin pages');