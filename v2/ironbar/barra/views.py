# -*- coding: utf-8 -*-

#from django.db.models import Q # Sirve para hacer varias consultas en el ORM
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.hashers import check_password
from django.contrib.auth import login as auth_login
from django.shortcuts import render, redirect
from django.views.generic import TemplateView
from django.utils.translation import gettext
from django.contrib.auth import authenticate
from django.utils.timezone import utc
from django.db.models import Max, Sum
from django.http import HttpResponse, StreamingHttpResponse
from heapq import merge

from management import *

import datetime
#import speech
import pyttsx
import thread
import json
import time
import re

from django.views.generic import TemplateView

#En un futuro esto habría que autodetectarlo o leerlo de un fichero de configuración.
conf_ip = load_from_configuration_file('ip')
conf_port = load_from_configuration_file('port')

def login(request):
	var = req_vars(request)

	if check_submit(request, 'login'):
		elements = ['email', 'password']
		if check_request(request, elements):
			email = request.POST['email']
			password = request.POST['password']
			user = authenticate(username=email, password=password)
			if (user is not None and user.is_staff) or (user is not None and user.is_active):
				auth_login(request, user)
				return redirect('/')
			else:
				msg = gettext("Incorrect user or password")
				context = {'msg':msg}
				return render(request, 'login.html', context)

	if check_submit(request, 'login_forget_password'):
		elements = ['recovery_password']
		if check_request(request, elements):
			try:
				usuario = Usuario.objects.get(email=var.get('recovery_password'))
			except:
				return HttpResponse("No existe el email")
			codigo_validacion = random.getrandbits(128)
			codigo_validacion = "ironbar" + str(codigo_validacion)[1:16]
			usuario.codigo_validacion = codigo_validacion
			usuario.save()
			thread.start_new_thread( 	send_mail,
										(	var.get('recovery_password'),
											"Ironbar v2 recovery password",
											"Hey " + usuario.nombre + ", if you want reset your password click here: http://" + conf_ip + ":" + conf_port + "/recovery/" + codigo_validacion
										)
									)
			msg = gettext("Check your email to reset your password")
			estado = "Successfull"
			context = {'msg':msg, 'estado':estado}
			return render(request, 'validate.html', context)

	form = AuthenticationForm()
	context = {}
	return render(request, 'login.html', context)

def logout(request):

	context = {}
	return do_login(request, 'logout.html', context, permisos_necesarios)

def index(request):

	permisos_necesarios = []
	context = {}
	return do_login(request, 'index.html', context, permisos_necesarios)

def administration(request):

	permisos_necesarios = ['Administration']
	context = {'dir':'administration'}
	return do_login(request, 'administration.html', context, permisos_necesarios)

def sales(request):
	var = req_vars(request)
	session = my_session(request.session)



	# Variables del filtro
	dt_init = DateTime_Controller("init")
	dt_finish = DateTime_Controller("finish")
	cliente_filtro = ""
	vendedor_filtro = ""

	lista_ventas = []
	ventas = None

	# Eliminar filtro
	if check_submit(request, 'sales_refresh'):
		try:
			session.delete('filtro')
			session.delete('datetime_init')
			session.delete('datetime_finish')
			session.delete('cliente_filtro')
			session.delete('vendedor_filtro')
		except:
			pass

	# Cargar filtro
	if session.is_exist('filtro'):
		dt_init.set_from_djangodb(session.get('datetime_init'))
		dt_finish.set_from_djangodb(session.get('datetime_finish'))
		cliente_filtro = session.get('cliente_filtro')
		vendedor_filtro = session.get('vendedor_filtro')

	# Aplicar filtro
	if check_submit(request, 'sales_search'):
		dt_init.set_from_datepicker(var.get('sales_init_date'), var.get('sales_init_hour'))
		dt_finish.set_from_datepicker(var.get('sales_finish_date'), var.get('sales_finish_hour'))
		cliente_filtro = var.get('sales_client')
		vendedor_filtro = var.get('sales_seller')

		# Guardar filtro
		session.put('datetime_init', dt_init.get_to_djangodb())
		session.put('datetime_finish', dt_finish.get_to_djangodb())
		session.put('cliente_filtro', cliente_filtro)
		session.put('vendedor_filtro', cliente_filtro)
		session.put('filtro', "1")

	# Cargar datos en la plantilla
	if (var.get('sales_init_date') and var.get('sales_init_hour')) or (var.get('sales_finish_date') or var.get('sales_finish_hour')) or var.get('sales_client') or var.get('sales_seller'):
		if session.is_exist('filtro') and check_submit(request, 'sales_search'):
			if dt_init.is_nulo():
				if not cliente_filtro:
					ventas = Factura.objects.filter(
						vendedor__in = Usuario.objects.filter(nombre__icontains=vendedor_filtro),
						fecha__gt = dt_finish.get_to_djangodb_datetime()
					)
				elif not vendedor_filtro:
					ventas = Factura.objects.filter(
						cliente__in = Usuario.objects.filter(nombre__icontains=cliente_filtro),
						fecha__gt = dt_finish.get_to_djangodb_datetime()
					)
				else:
					ventas = Factura.objects.filter(
						cliente__in = Usuario.objects.filter(nombre__icontains=cliente_filtro),
						vendedor__in = Usuario.objects.filter(nombre__icontains=vendedor_filtro),
						fecha__gt = dt_finish.get_to_djangodb_datetime()
					)
			elif dt_finish.is_nulo():
				if not cliente_filtro:
					ventas = Factura.objects.filter(
						vendedor__in = Usuario.objects.filter(nombre__icontains=vendedor_filtro),
						fecha__gt = dt_init.get_to_djangodb_datetime()
					)
				elif not vendedor_filtro:
					ventas = Factura.objects.filter(
						cliente__in = Usuario.objects.filter(nombre__icontains=cliente_filtro),
						fecha__gt = dt_init.get_to_djangodb_datetime()
					)
				else:
					ventas = Factura.objects.filter(
						cliente__in = Usuario.objects.filter(nombre__icontains=cliente_filtro),
						vendedor__in = Usuario.objects.filter(nombre__icontains=vendedor_filtro),
						fecha__gt = dt_init.get_to_djangodb_datetime()
					)
			else:
				if not cliente_filtro:
					ventas = Factura.objects.filter(
						vendedor__in = Usuario.objects.filter(nombre__icontains=vendedor_filtro),
						fecha__range = [dt_init.get_to_djangodb_datetime(), dt_finish.get_to_djangodb_datetime()]
					)
				elif not vendedor_filtro:
					ventas = Factura.objects.filter(
						cliente__in = Usuario.objects.filter(nombre__icontains=cliente_filtro),
						fecha__range = [dt_init.get_to_djangodb_datetime(), dt_finish.get_to_djangodb_datetime()]
					)
				else:
					ventas = Factura.objects.filter(
						cliente__in = Usuario.objects.filter(nombre__icontains=cliente_filtro),
						vendedor__in = Usuario.objects.filter(nombre__icontains=vendedor_filtro),
						fecha__range = [dt_init.get_to_djangodb_datetime(), dt_finish.get_to_djangodb_datetime()]
					)
		else:
			ventas = Factura.objects.all()
	else:
		ventas = Factura.objects.all()

	paginator = Paginator(ventas, 4)
	page = var.get('page')

	try:
		paginacion = paginator.page(page)
	except PageNotAnInteger:
		paginacion = paginator.page(1)
	except EmptyPage:
		paginacion = paginator.page(paginator.num_pages)

	for factura in paginacion:
		lista_ventas.append((factura, Linea.objects.filter(factura=factura)))

	permisos_necesarios = ['Administration', 'Sales']
	context = {'dir':'sales', 'lista_ventas':lista_ventas, 'paginas':range(1,paginator.num_pages+1), 'paginacion':paginacion}
	return do_login(request, 'sales.html', context, permisos_necesarios)
	
def products(request):
	msg = ""
	var = req_vars(request)
	productos = Producto.objects.all()
	tipo_precios = Tipo_Precio.objects.all().order_by('nombre')
	
	# Añadir productos
	if check_submit(request, 'products_add'):
		elements = ['products_add_stock','products_add_name']
		if check_request(request, elements):
			if check_request(request, ['products_add_barcode']):
				barcode = var.get('products_add_barcode')
			else:
				barcode = generate_barcode()
			try:
				Producto.objects.get(codigo_de_barras=barcode)				
				msg = gettext("This barcode is being used.")	
			except:
				try:
					Producto.objects.get(nombre=var.get('products_add_name'))
					msg = gettext("This name is being used.")
				except:
					producto = Producto(codigo_de_barras=barcode, nombre=var.get('products_add_name'), stock=var.get('products_add_stock'))
					producto.save()
					for item in request.POST:
						if item.find('products_add_prize_') != -1:
							tipo_precio = Tipo_Precio.objects.get(nombre=item.split('products_add_prize_')[1])
							precio = Precio(producto=producto, tipo_precio=tipo_precio, precio=var.get(item))
							precio.save()
			

	# Editar un producto
	if check_submit(request, 'products_edit'):
		elements = ['products_edit_old_barcode', 'products_edit_barcode', 'products_edit_name', 'products_edit_stock']
		if check_request(request, elements):
			producto = Producto.objects.get(codigo_de_barras=var.get('products_edit_old_barcode'))
			producto.codigo_de_barras = var.get('products_edit_barcode')
			producto.nombre = var.get('products_edit_name')
			producto.stock = int(var.get('products_edit_stock')) + producto.stock
			producto.save()

			precios = Precio.objects.filter(producto=producto).order_by('tipo_precio')
			for precio in precios:
				precio.precio = float(var.get('products_edit_prize_' + precio.tipo_precio.nombre))
				precio.save()

	# Eliminar un producto
	if check_submit(request, 'product_delete'):
		elements = ['product_variable_borrar']
		if check_request(request, elements):
			producto = Producto.objects.get(codigo_de_barras=var.get('product_variable_borrar'))
			producto.delete()


	# Filtrar productos
	if check_submit(request, 'products_filter_search'):
		productos = []

		filtro_barcode = var.get('products_filter_search_barcode')
		filtro_name = var.get('products_filter_search_name')

		print filtro_barcode
		print filtro_name

		for producto in Producto.objects.all():
			if filtro_barcode == "":
				if re.search(re.compile(filtro_name, re.IGNORECASE), producto.nombre):
					productos.append(producto)
			elif filtro_name == "":
				if re.search(re.compile(filtro_barcode, re.IGNORECASE), producto.codigo_de_barras):
					productos.append(producto)
			else:
				if re.search(re.compile(filtro_barcode, re.IGNORECASE), producto.codigo_de_barras) and re.search(re.compile(filtro_name, re.IGNORECASE), producto.nombre):
					productos.append(producto)

    # Muestra los productos con sus precios y hace la paginación
	paginator = Paginator(productos, 9)
	page = request.GET.get('page')
	lista_productos_precios = []

	try:
		lista_productos = paginator.page(page)
	except PageNotAnInteger:
		lista_productos = paginator.page(1)
	except EmptyPage:
		lista_productos = paginator.page(paginator.num_pages)

	for producto in lista_productos:
		lista_precios = []
		precios = Precio.objects.filter(producto=producto).order_by('tipo_precio__nombre')
		for precio in precios:
			lista_precios.append(precio)
		tupla = (producto, lista_precios)
		lista_productos_precios.append(tupla)

	permisos_necesarios = ['Administration', 'Products']
	context = {'dir':'products', 'type_of_prize_list':tipo_precios, 'lista_productos_precios':lista_productos_precios, 'tipo_precios':tipo_precios,'paginas':range(1,paginator.num_pages+1), 'lista_productos':lista_productos, 'msg':msg}
	return do_login(request, 'products.html', context, permisos_necesarios)

def expenses(request):
	var = req_vars(request)
	lista_ventas = [] # facturas, lineas
	expenses = 0
	discharged = 0

	try:
		facturas = Factura.objects.filter(cliente=request.user)
	except:
		facturas = []

	for factura in facturas:
		lista_ventas.append((factura, Linea.objects.filter(factura=factura)))
		if not factura.pagada:
			expenses = expenses + factura.importe
		else:
			discharged = discharged + factura.importe
	

	paginator = Paginator(lista_ventas, 4)
	page = var.get('page')

	try:
		paginacion = paginator.page(page)
	except PageNotAnInteger:
		paginacion = paginator.page(1)
	except EmptyPage:
		paginacion = paginator.page(paginator.num_pages)

	permisos_necesarios = []
	context = {'lista_ventas':lista_ventas, 'paginas':range(1,paginator.num_pages+1), 'paginacion':paginacion, 'expenses':expenses, 'discharged':discharged}
	return do_login(request, 'expenses.html', context, permisos_necesarios)

def profile(request):
	var = req_vars(request)
	msg = ""

	try:
		permisos_usuarios = Permiso_Usuario.objects.filter(usuario=request.user)
		permisos_grupos = Permiso_Grupo.objects.filter(grupo=request.user.grupo)
		tipo_permisos = list(merge(permisos_usuarios, permisos_grupos))
	except:
		# Pasa por aquí si no está logueado
		tipo_permisos = []

	if check_submit(request, 'profile_accept'):
		elements = ['profile_nombre', 'profile_apellidos', 'profile_email', 'profile_dni', 'profile_old_pass']
		if check_request(request, elements):
			if check_password(var.get('profile_old_pass'), request.user.password):
				u = request.user
				u.nombre = var.get('profile_nombre')
				u.apellidos = var.get('profile_apellidos')
				u.email = var.get('profile_email')
				u.dni = var.get('profile_dni')
				if var.get('profile_pass1')!="" or var.get('profile_pass2'):
					if var.get('profile_pass1') == var.get('profile_pass2'):
						u.set_password(var.get('profile_pass1'))
						u.save()
					else:
						msg = gettext("Password must be same.")
			else:
				msg = gettext("Your old password is incorrect.")

	permisos_necesarios = []
	context = {'msg':msg,'tipo_permisos':tipo_permisos}
	return do_login(request, 'profile.html', context, permisos_necesarios)

def sell(request):
	var = req_vars(request)
	session = my_session(request.session)
	lista_clientes = []
	lista_productos = [] # (producto, precio)

	# Cargar datos desde la sesión
	tipo_precio_efectivo = Tipo_Precio.objects.get(nombre=session.get_if_exist('tipo_precio_efectivo_nombre', Tipo_Precio.objects.get(default=True)))
	cliente_seleccionado_email = session.get_if_exist('cliente_email',"")
	filtro_productos = session.get_if_exist('filtro_productos',"")
	filtro_clientes = session.get_if_exist('filtro_clientes',"")
	cesta = session.get_if_exist('cesta',[])

	cart = Cart(tipo_precio_efectivo)
	cart.load(deserializar_cesta(cesta))

	# Añadir al carro
	if check_submit(request, 'sell_to_cart'):
		carro_vacio = "No"
		producto = Producto.objects.get(codigo_de_barras=var.get('sell_to_cart'))
		precio = Precio.objects.get(producto=producto, tipo_precio=tipo_precio_efectivo)
		cart.add(producto, precio, 1)

	# Eliminar del carro
	if check_submit(request, 'sell_delete_from_cart'):
		codigo_de_barras = var.get('sell_delete_from_cart')
		cart.remove(codigo_de_barras)

	# Cambiar cliente
	if check_submit(request, 'small'):
		if var.get('small')!="None":
			usuario = Usuario.objects.get(email=var.get('small'))
			cliente_seleccionado_email = usuario.email
			grupo = usuario.grupo
			if grupo:
				tipo_precio_efectivo = grupo.tipo_precio
			else:
				tipo_precio_efectivo = Tipo_Precio.objects.get(default=True)
		else:
			cliente_seleccionado_email = ""
			tipo_precio_efectivo = Tipo_Precio.objects.get(default=True)
		cart.set_tipo_precio_efectivo(tipo_precio_efectivo)
		cart.refresh()

	# Filtrar productos
	if check_submit(request, 'sell_filter_products'):
		filtro_productos = var.get('sell_filter_products')

	# Filtrar clientes
	if check_submit(request, 'sell_filter_clients'):
		filtro_clientes = var.get('sell_filter_clients')
		
	# Vender
	if check_submit(request, 'sell_finish'):
		if cart.total()!=0:
			if cliente_seleccionado_email!="":
				factura = Factura(
					fecha = datetime.datetime.utcnow().replace(tzinfo=utc), 
					#fecha = datetime.datetime.utcnow(), 
					importe = cart.total(),
					cliente = Usuario.objects.get(email=cliente_seleccionado_email), 
					vendedor= request.user, 
					pagada = check_submit(request, 'sell_paynow'), 
					pedido_entregado = not check_submit(request, 'sell_takeorder')
				)
				factura.save()
			else:
				factura = Factura(
					fecha = datetime.datetime.utcnow().replace(tzinfo=utc), 
					#fecha = datetime.datetime.utcnow(), 
					importe = cart.total(),
					vendedor= request.user, 
					pagada = check_submit(request, 'sell_paynow'), 
					pedido_entregado = not check_submit(request, 'sell_takeorder')
				)
				factura.save()
			for linea in cart.get():
				linea[0].stock = linea[0].stock - linea[2]
				linea[0].save()
				linea = Linea(
					producto_codigo_de_barras = linea[0].codigo_de_barras,
					producto_nombre = linea[0].nombre,
					producto_cantidad_vendida = linea[2],
					producto_precio = linea[1].precio,
					factura = factura
				)
				linea.save()
		cart.clear()
		tipo_precio_efectivo = Tipo_Precio.objects.get(default=True)
                cart.set_tipo_precio_efectivo(tipo_precio_efectivo)
                cart.refresh()
		cliente_seleccionado_email = ""

	# Guardar datos en la sesion
	session.put('cesta', serializar_cesta(cart.get()))
	session.put('tipo_precio_efectivo_nombre', tipo_precio_efectivo.nombre)
	session.put('cliente_email', cliente_seleccionado_email)
	session.put('filtro_clientes', filtro_clientes)
	session.put('filtro_productos', filtro_productos)

	# Carga de datos en la plantilla
	for producto in Producto.objects.filter(nombre__icontains=filtro_productos):
		precio = Precio.objects.get(producto=producto, tipo_precio=tipo_precio_efectivo)
		lista_productos.append((producto, precio))
	for usuario in Usuario.objects.all():
		if re.search(re.compile(filtro_clientes, re.IGNORECASE), usuario.nombre + " " + usuario.apellidos) or re.search(re.compile(filtro_clientes, re.IGNORECASE), usuario.email):
			lista_clientes.append(usuario)

	permisos_necesarios = ['Administration', 'Sell']
	context = {'dir':'sell', 'lista_clientes':lista_clientes, 'lista_productos':lista_productos, 'cliente_seleccionado_email':cliente_seleccionado_email, 'cesta':cart.get(), 'total':cart.total()}
	return do_login(request, 'sell.html', context, permisos_necesarios)

def liabilities(request):
	var = req_vars(request)
	session = my_session(request.session)

	lista_deudas = [] # (usuario, importe)
	lista_deudas_a_pagar = [] # factura
	lista_deudas_a_pagar_copia = []
	lista_deudas_marcadas = []
	importe = 0
	importe_total = 0
	deudores = 0
	tenia_deuda = False
	lista_usuarios = []
	lista_email_usuarios_filtrar = []
	total_to_charge = 0 # Importe de facturas marcadas

	# Cargar de la sesion
	tag_actual = session.get_if_exist('tag_actual_liabilities', "tab1")
	lista_email_usuarios_filtrar = session.get_if_exist('lista_email_usuarios_filtrar', [])
	for email_usuario in lista_email_usuarios_filtrar:
		usuario = Usuario.objects.get(email=email_usuario)
		for factura in Factura.objects.filter(cliente=usuario, pagada=False):
			lista_deudas_a_pagar.append(factura)

	# Limpiar la pantalla - Charge
	if check_submit(request, 'liabilities_charge_clean'):
		if session.is_exist('lista_email_usuarios_filtrar'):
			session.delete('lista_email_usuarios_filtrar')
			lista_email_usuarios_filtrar = []
			lista_deudas_a_pagar = []

	# Añadir deudas a la lista - Charge
	if check_submit(request, 'liabilities_load_user_temp'):
		tag_actual = "tab2"
		usuario = Usuario.objects.get(email=var.get('liabilities_load_user_temp'))
		for factura in Factura.objects.filter(cliente=usuario, pagada=False):
			if factura not in lista_deudas_a_pagar:
				lista_deudas_a_pagar.append(factura)
		if usuario.email not in lista_email_usuarios_filtrar:
			lista_email_usuarios_filtrar.append(usuario.email)

	# Cargar todas las deudas - General View
	for usuario in Usuario.objects.all():
		importe = 0
		tenia_deuda = False
		for factura in Factura.objects.filter(cliente=usuario, pagada=False):
			importe = importe + factura.importe
			importe_total = importe_total + factura.importe
			tenia_deuda = True
		if tenia_deuda:
			tenia_deuda = False
			deudores = deudores + 1
			lista_usuarios.append(usuario)
			lista_deudas.append((usuario, importe))

	# Filtrar deudas - General View
	if check_submit(request, 'liabilities_filter'):
		tag_actual = "tab1"
		elements = ['liabilities_name_filter']
		if check_request(request, elements):
			lista_deudas = []
			for usuario in Usuario.objects.all():
				importe = 0
				if re.search(re.compile(var.get('liabilities_name_filter'), re.IGNORECASE), usuario.nombre + " " + usuario.apellidos) or re.search(re.compile(var.get('liabilities_name_filter'), re.IGNORECASE), usuario.email):
					for factura in Factura.objects.filter(cliente=usuario, pagada=False):
						importe = importe + factura.importe
					if importe!=0:
						lista_deudas.append((usuario, importe))

	# Cobrar - Charge
	if check_submit(request, 'liabilities_charge'):
		for item in request.POST:
			if item.find('liabilities_charge_checkbox_') != -1:
				lista_deudas_marcadas.append(Factura.objects.get(id=item.split('liabilities_charge_checkbox_')[1]))

		for factura in lista_deudas_marcadas:
			lista_deudas_a_pagar.remove(factura)
			factura.pagada = True
			factura.save()

	# Guardar en la sesion
	session.put('lista_email_usuarios_filtrar', lista_email_usuarios_filtrar)
	session.put('tag_actual_liabilities', tag_actual)

	# Paginación
	paginator = Paginator(lista_deudas, 9)
	page = request.GET.get('page')

	try:
		lista_deudas_paginada = paginator.page(page)
	except PageNotAnInteger:
		lista_deudas_paginada = paginator.page(1)
	except EmptyPage:
		lista_deudas_paginada = paginator.page(paginator.num_pages)

	permisos_necesarios = ['Administration', 'Liabilities']
	context = {'dir':'liabilities', 'lista_deudas_paginada':lista_deudas_paginada, 'importe_total':importe_total, 'deudores':deudores, 'paginas':range(1,paginator.num_pages+1), 'lista_usuarios':lista_usuarios, 'lista_deudas_a_pagar':lista_deudas_a_pagar, 'tag_actual':tag_actual, 'total_to_charge':total_to_charge}
	return do_login(request, 'liabilities.html', context, permisos_necesarios)

def billing(request):
	lista_eventos = [] # evento, evento_earnings, evento_expenses, evento_total, lista_resumen_evento
	lista_resumen_evento = [] # nombre_producto, total_vendido, ganancias, gastos, ganancias_totales
	lista_resumen = [] # nombre_producto, total_vendido, ganancias, gastos, ganancias_totales
	earnings = 0
	expenses = 0
	tipo_precio = None

	try:
		tipo_precio = Tipo_Precio.objects.get(cost=True)
		is_precio_coste = True
	except:
		is_precio_coste = False

	# Billing por evento
	for evento in Evento.objects.all():
		lista_resumen_evento = []
		ganancias = 0
		earnings = 0
		expenses = 0
		for factura in Factura.objects.filter(fecha__range=(evento.fecha_inicio, evento.fecha_final)):
			for line in Linea.objects.filter(factura=factura).values('producto_nombre','producto_precio').annotate(total_vendido=Sum('producto_cantidad_vendida')).order_by('producto_nombre','producto_precio'):		
				producto_nombre = line.get('producto_nombre')
				total_vendido = line.get('total_vendido')
				producto_precio = line.get('producto_precio')
				ganancias = producto_precio*total_vendido
				if is_precio_coste:
					try:
						producto = Producto.objects.get(nombre=producto_nombre)
						gastos = Precio.objects.get(producto=producto, tipo_precio=tipo_precio).precio*total_vendido
						ganancia_t = ganancias - gastos
					except:
						gastos = 0
						ganancia_t = 0
				else:
					gastos = 0
					ganancia_t = 0

				earnings = earnings + ganancias
				expenses = expenses + gastos

				# Junta productos repetidos
				encontrado = False
				lista_resumen_copia = lista_resumen_evento
				for item in lista_resumen_copia:
					if item[0]==producto_nombre:
						encontrado = True
						lista_resumen_evento.remove(item)
						lista_resumen_evento.append(( producto_nombre, total_vendido+item[1], ganancias+item[2], gastos+item[3], ganancia_t+item[4] ))

				if not encontrado:
					lista_resumen_evento.append(( producto_nombre, total_vendido, ganancias, gastos, ganancia_t ))

		lista_eventos.append((evento, earnings, expenses, earnings-expenses, lista_resumen_evento))

		# Genera gráfica de tarta de los eventos
		chart_nombres = []
		chart_ventas = []
		chart_earnings = []
		chart_expenses = []
		chart_total = []
		for producto in lista_resumen_evento:
			chart_nombres.append(producto[0].encode('utf-8').replace("ó","o").replace("á","a").replace("é","e").replace("í","i").replace("ú","u"))
			chart_ventas.append(producto[1])
			chart_earnings.append(producto[2])
			chart_expenses.append(producto[3])
			chart_total.append(producto[4])

		generarGraficaTarta(250, 100, chart_nombres, chart_ventas, "tartaVentas" + str(evento.id))
		generarGraficaTarta(600, 250, chart_nombres, chart_ventas, "tartaVentas" + str(evento.id) + "HD")
		generarGraficaTarta(600, 250, chart_nombres, chart_earnings, "tartaEarnings" + str(evento.id) + "HD")
		generarGraficaTarta(600, 250, chart_nombres, chart_expenses, "tartaExpenses" + str(evento.id) +"HD")
		generarGraficaTarta(600, 250, chart_nombres, chart_total, "tartaTotal" + str(evento.id) + "HD")


	# Billing Total
	ganancias = 0
	earnings = 0
	expenses = 0
	for line in Linea.objects.values('producto_nombre','producto_precio').annotate(total_vendido=Sum('producto_cantidad_vendida')).order_by('producto_nombre','producto_precio'):
		producto_nombre = line.get('producto_nombre')
		total_vendido = line.get('total_vendido')
		producto_precio = line.get('producto_precio')
		ganancias = producto_precio*total_vendido
		if is_precio_coste:
			try:
				producto = Producto.objects.get(nombre=producto_nombre)
				gastos = Precio.objects.get(producto=producto, tipo_precio=tipo_precio).precio*total_vendido
				ganancia_t = ganancias - gastos
			except:
				gastos = 0
				ganancia_t = 0
		else:
			gastos = 0
			ganancia_t = 0

		earnings = earnings + ganancias
		expenses = expenses + gastos

		# Junta productos repetidos
		encontrado = False
		lista_resumen_copia = lista_resumen
		for item in lista_resumen_copia:
			if item[0]==producto_nombre:
				encontrado = True
				lista_resumen.remove(item)
				lista_resumen.append(( producto_nombre, total_vendido+item[1], ganancias+item[2], gastos+item[3], ganancia_t+item[4] ))
		if not encontrado:
			lista_resumen.append(( producto_nombre, total_vendido, ganancias, gastos, ganancia_t ))

	# Genera gráfica de tarta totales
	chart_nombres = []
	chart_ventas = []
	chart_earnings = []
	chart_expenses = []
	chart_total = []
	for producto in lista_resumen:
		chart_nombres.append(producto[0].encode('utf-8').replace("ó","o").replace("á","a").replace("é","e").replace("í","i").replace("ú","u"))
		chart_ventas.append(producto[1])
		chart_earnings.append(producto[2])
		chart_expenses.append(producto[3])
		chart_total.append(producto[4])

	generarGraficaTarta(250, 100, chart_nombres, chart_ventas, "tartaVentas")
	generarGraficaTarta(600, 250, chart_nombres, chart_ventas, "tartaVentasHD")
	generarGraficaTarta(600, 250, chart_nombres, chart_earnings, "tartaEarningsHD")
	generarGraficaTarta(600, 250, chart_nombres, chart_expenses, "tartaExpensesHD")
	generarGraficaTarta(600, 250, chart_nombres, chart_total, "tartaTotalHD")

	permisos_necesarios = ['Administration', 'Billing']
	context = {'dir':'billing', 'lista_resumen':lista_resumen, 'is_precio_coste':is_precio_coste, 'earnings':earnings, 'expenses':expenses, 'total':earnings-expenses, 'lista_eventos':lista_eventos}
	return do_login(request, 'billing.html', context, permisos_necesarios)

def messages(request):

	permisos_necesarios = ['Administration']
	context = {'dir':'messages'}
	return do_login(request, 'messages.html', context, permisos_necesarios)

def no_permission(request):

	context = {}
	return do_login(request, 'no_permission.html', context)

def prize_management(request):
	elements = ['prize_name', 'prize_description']
	var = req_vars(request)
	precios = Tipo_Precio.objects.all()
	msg = ""

	# Crear un tipo de precio
	if check_submit(request, 'prize_create'):
		lista_precios_max_por_producto = Precio.objects.values('producto').annotate(Max('precio'))
		default = False
		cost = False

		#if precios or check_submit(request, 'prize_new_default'):
		if precios or request.POST.has_key('prize_new_default'):
			if request.POST.has_key('prize_new_default'):
				default = True
				if precios:
					try:
						tipo_precio = Tipo_Precio.objects.get(default=True)
						tipo_precio.default = False
						tipo_precio.save()
					except:
						pass
			else:
				default = False
			if request.POST.has_key('prize_new_cost'):
				cost = True
				if precios:
					try:
						tipo_precio = Tipo_Precio.objects.get(cost=True)
						tipo_precio.cost = False
						tipo_precio.save()
					except:
						pass
			else:
				cost = False

			tipo_precio = Tipo_Precio(nombre=var.get('prize_name'), descripcion=var.get('prize_description'), default=default, cost=cost)
			tipo_precio.save()

			for precio_max in lista_precios_max_por_producto:
				producto = Producto.objects.get(id=precio_max['producto'])
				precio = Precio(producto=producto, tipo_precio=tipo_precio, precio=precio_max['precio__max'])
				precio.save()
		else:
			msg = gettext("You need at least 1 default price.")

	# Borrar un tipo de precio
	if check_submit(request, 'prize_delete'):
		tipo_precio = Tipo_Precio.objects.get(nombre=var.get('prize_variable_borrar'))
		if tipo_precio.default:
			msg = gettext("You can't delete the default prize.")
		else:
			tipo_precio.delete()

	# Editar un tipo de precio
	if check_submit(request, 'prize_edit'):
		precio = Tipo_Precio.objects.get(nombre=var.get('prize_edit_name_temp'))
		precio.nombre = var.get('prize_edit_name')
		precio.descripcion = var.get('prize_edit_description')
		
		if precio.default:
			if request.POST.has_key('prize_edit_default'):
				precio.save()
			else:
				msg = gettext("You can't unset the default prize.")
		else:
			if request.POST.has_key('prize_edit_default'):
				precio_default = Tipo_Precio.objects.get(default=True)
				precio_default.default = False
				precio_default.save()
				precio.default = True
				precio.save()
			else:
				precio.default = False
				precio.save()

		if request.POST.has_key('prize_edit_cost'):
			try:
				precio_default = Tipo_Precio.objects.get(cost=True)
				precio_default.cost = False
				precio_default.save()
			except:
				pass
			precio.cost = True
			precio.save()
		else:
			precio.cost = False
			precio.save()

	precios = Tipo_Precio.objects.all()
	permisos_necesarios = ['Administration']
	context = {'dir':'prize_management','lista_precios':precios,'msg':msg}
	return do_login(request, 'prize_management.html', context, permisos_necesarios)

def user_management(request):
	var = req_vars(request)
	session = my_session(request.session)
	permisos = Permiso.objects.all()
	grupos = Grupo.objects.all()
	usuarios = Usuario.objects.all()
	msg_create = ""
	msg_edit = ""
	msg_delete = ""

	# Cargar desde la sesión
	tag_actual = session.get_if_exist('tag_actual_user', "tab1")

	# Crear un usuario
	if check_submit(request, 'user_create'):
		tag_actual = "tab1"
		elements = ['user_create_name','user_create_surname','user_create_dni','user_create_email','user_create_password','user_create_group_temp','user_create_permission_temp']
		if check_request(request, elements):
			usuario = Usuario(nombre=var.get('user_create_name'), apellidos=var.get('user_create_surname'), dni=var.get('user_create_dni'), email=var.get('user_create_email'))
			usuario.set_password(var.get('user_create_password'))
			usuario.is_active = True
			if 'user_create_superuser' in request.POST.keys():
				usuario.is_staff = True
			if var.get('user_create_group_temp') != "No Group":
				grupo = Grupo.objects.get(nombre=var.get('user_create_group_temp'))
				usuario.grupo = grupo
			usuario.save()
			for item in var.get('user_create_permission_temp').split(';;;')[1:len(var.get('user_create_permission_temp').split(';;;'))]:
				if item!="Login":
					permiso = Permiso.objects.get(nombre=item)
					permiso_usuario = Permiso_Usuario(permiso=permiso, usuario=usuario)
					permiso_usuario.save()

	# Modificar un usuario
	if check_submit(request, 'user_update'):
		tag_actual = "tab2"
		elements = ['user_edit_name', 'user_edit_surname', 'user_edit_dni', 'user_edit_email', 'user_edit_email_temp']
		if check_request(request, elements):
			usuario = Usuario.objects.get(email=var.get("user_edit_email_temp"))
			usuario.email = var.get("user_edit_email")
			usuario.nombre = var.get("user_edit_name")
			usuario.apellidos = var.get("user_edit_surname")
			usuario.dni = var.get("user_edit_dni")
			
			if var.get("user_edit_password") != None:
				usuario.set_password = var.get("user_edit_password")

			usuario.save()

	# Eliminar un usuario
	if check_submit(request, 'user_delete'):
		tag_actual = "tab3"
		elements = ['user_delete_email_temp']
		if check_request(request, elements):
			usuario = Usuario.objects.get(email=var.get('user_delete_email_temp'))
			if Factura.objects.filter(cliente=usuario, pagada=False) == 0:
				usuario.delete()
			else:
				msg_delete = gettext("You can not delete that user, has outstanding debts")

	# Guardar en la sesion
	session.put('tag_actual_user', tag_actual)

	permisos_necesarios = ['Administration']
	context = {'dir':'user_management','lista_permisos':permisos,'lista_grupos':grupos,'lista_usuarios':usuarios,'tag_actual':tag_actual,'msg_create':msg_create,'msg_edit':msg_edit,'msg_delete':msg_delete}
	return do_login(request, 'user_management.html', context, permisos_necesarios)

def group_management(request):
	var = req_vars(request)
	session = my_session(request.session)
	precios = Tipo_Precio.objects.all()
	permisos = Permiso.objects.all()
	grupos = Grupo.objects.all()
	usuarios = Usuario.objects.all()

	# Cargar desde la sesión
	tag_actual = session.get_if_exist('tag_actual_group', "tab1")

	# Eliminar un grupo
	if check_submit(request, 'group_delete'):
		tag_actual = "tab4"
		elements = ['group_delete_name_temp']
		if check_request(request, elements):
			grupo = Grupo.objects.get(nombre=var.get('group_delete_name_temp'))
			# Hay que desasignar el grupo a los usuarios antes de borrarlo
			for usuario in Usuario.objects.filter(grupo=grupo):
				usuario.grupo = None;
				usuario.save()
			grupo.delete()


	# Editar un grupo
	if check_submit(request, 'group_update'):
		tag_actual = "tab3"
		elements = ['group_edit_name','group_edit_description','group_edit_name_temp']
		if check_request(request, elements):
			grupo = Grupo.objects.get(nombre=var.get('group_edit_name_temp'))
			grupo.nombre = var.get("group_edit_name")
			grupo.descripcion = var.get("group_edit_description")
			grupo.save()

	# Asignar un grupo
	if check_submit(request, 'group_assign'):
		tag_actual = "tab1"
		elements = ['group_assign_radio', 'group_assign_email_temp']
		if check_request(request, elements):
			usuario = Usuario.objects.get(email=var.get('group_assign_email_temp'))
			try:
				grupo = Grupo.objects.get(nombre=var.get('group_assign_radio'))
			except:
				grupo = None
			usuario.grupo = grupo
			usuario.save()

	# Crear un grupo
	if check_submit(request, 'group_create'):
		tag_actual = "tab2"
		elements = ['group_create_name','group_create_description','group_create_prize_temp','group_create_permission_temp']
		if check_request(request, elements):
			prize = Tipo_Precio.objects.get(nombre=var.get('group_create_prize_temp'))
			grupo = Grupo(nombre=var.get('group_create_name'), descripcion=var.get('group_create_description'), tipo_precio=prize)
			grupo.save()

			for item in var.get('group_create_permission_temp').split(';;;')[1:len(var.get('group_create_permission_temp').split(';;;'))]:
				if item!="Login":
					permiso = Permiso.objects.get(nombre=item)
					permiso_grupo = Permiso_Grupo(permiso=permiso, grupo=grupo)
					permiso_grupo.save()

	# Guardar en la sesion
	session.put('tag_actual_group', tag_actual)

	permisos_necesarios = ['Administration']
	context = {'dir':'group_management','lista_precios':precios,'lista_permisos':permisos,'lista_grupos':grupos,'lista_usuarios':usuarios,'tag_actual':tag_actual}
	return do_login(request, 'group_management.html', context, permisos_necesarios)

def permission_management(request):
	var = req_vars(request)
	grupos = Grupo.objects.all()
	usuarios = Usuario.objects.all()
	permisos = Permiso.objects.all()
	usuarios_permisos = [] # Lista de tuplas (usuario, permisos)
	grupos_permisos = [] # Lista de tuplas (grupo, permisos)

	lista_permiso_usuario = []
	lista_permiso_grupo = []
	elements = []

	# Asignar permisos a usuarios
	if check_submit(request, 'assign_permission_to_users'):
		elements = ['permission_assign_user_email_div_text_temp']
		if check_request(request, elements):
			usuario = Usuario.objects.get(email=var.get('permission_assign_user_email_div_text_temp'))
			lista_permiso_usuario = Permiso_Usuario.objects.filter(usuario=usuario)
			for permiso_usuario in lista_permiso_usuario:
				permiso_usuario.delete()
			for item in request.POST:
				if item.find('permission_checkbox_') != -1:
					permiso = Permiso.objects.get(nombre=item.split('permission_checkbox_')[1])
					Permiso_Usuario(permiso=permiso, usuario=usuario).save()

	# Asignar permisos a grupos
	if check_submit(request, 'assign_permission_to_groups'):
		elements = ['permission_assign_group_name_div_text_temp']
		if check_request(request, elements):
			grupo = Grupo.objects.get(nombre=var.get('permission_assign_group_name_div_text_temp'))
			lista_permiso_grupo = Permiso_Grupo.objects.filter(grupo=grupo)
			for permiso_grupo in lista_permiso_grupo:
				permiso_grupo.delete()
			for item in request.POST:
				if item.find('permission_group_checkbox_') != -1:
					permiso = Permiso.objects.get(nombre=item.split('permission_group_checkbox_')[1])
					Permiso_Grupo(permiso=permiso, grupo=grupo).save()

	# Cargar los datos en el template
	for usuario in usuarios:
		lista_permiso_usuario = []
		for permiso in permisos:
			try:
				Permiso_Usuario.objects.get(usuario=usuario, permiso=permiso)
				lista_permiso_usuario.append(permiso)
			except:
				pass
		tupla = (usuario, lista_permiso_usuario)
		usuarios_permisos.append(tupla)

	for grupo in grupos:
		lista_permiso_grupo = []
		for permiso in permisos:
			try:
				Permiso_Grupo.objects.get(grupo=grupo, permiso=permiso)
				lista_permiso_grupo.append(permiso)
			except:
				pass
		tupla = (grupo, lista_permiso_grupo)
		grupos_permisos.append(tupla)

	permisos_necesarios = ['Administration']
	context = {'dir':'permission_management','lista_grupos':grupos,'lista_usuarios':usuarios,'lista_permisos':permisos,'lista_usuarios_permisos':usuarios_permisos,'lista_grupos_permisos':grupos_permisos}
	return do_login(request, 'permission_management.html', context, permisos_necesarios)

def backup_management(request):

	permisos_necesarios = ['Administration']
	context = {'dir':'backup_management'}
	return do_login(request, 'backup_management.html', context, permisos_necesarios)

def log_management(request):

	permisos_necesarios = ['Administration']
	context = {'dir':'log_management'}
	return do_login(request, 'log_management.html', context, permisos_necesarios)

def db_management(request):

	permisos_necesarios = ['Administration']
	context = {'dir':'db_management'}
	return do_login(request, 'db_management.html', context, permisos_necesarios)

def register(request):
	elements = ['register_name','register_surname','register_email','register_dni','register_pass1','register_pass2']
	var = req_vars(request)
	msg_dni = ""
	msg_pass = ""
	msg_email = ""

	# Registrar un usuario
	if check_submit(request, 'register_submit'):
		if check_request(request, elements):
			if Usuario.objects.filter(dni=var.get("register_dni")).count()==0:
				if Usuario.objects.filter(email=var.get("register_email")).count()==0:
					if var.get('register_pass1') == var.get('register_pass2'):
						codigo_validacion = random.getrandbits(128)
						codigo_validacion = "ironbar" + str(codigo_validacion)[1:16]
						thread.start_new_thread( 	send_mail,
													(	var.get('register_email'),
														"Ironbar v2 registration",
														"Hey " + var.get('register_name') + ", you have successfully registered. Validate your email here: http://" + conf_ip + ":" + conf_port + "/validate/" + codigo_validacion
													)
												)
						usuario = Usuario(nombre=var.get('register_name'), apellidos=var.get('register_surname'), dni=var.get('register_dni'), email=var.get('register_email'))
						usuario.set_password(var.get('register_pass1'))
						usuario.codigo_validacion = codigo_validacion
						usuario.save()
						return render(request, 'success_registered.html', {})
					else:
						msg_pass = gettext("Password must be equals")
				else:
					msg_email = gettext("Email that already exists.")
			else:
				msg_dni = gettext("DNI that already exists.")
		else:
			error("Error form.")

	context = {"msg_dni":msg_dni,"msg_pass":msg_pass, "msg_email":msg_email}
	return render(request, 'register.html', context)

def register_dni_ajax(request):
	var = req_vars(request)
	patron = re.compile('[0-9]{8}[a-z|A-Z]+')
	data = "no"

	if len(var.get("dni"))==9:
		if patron.search(var.get("dni")):
			data = "ok"

		if Usuario.objects.filter(dni=var.get("dni")).count()!=0:
			data = "repetido"
			return StreamingHttpResponse(data)
	
	return StreamingHttpResponse(data)

def register_pass1_ajax(request):
	var = req_vars(request)
	patron = re.compile('[a-z|A-Z]{5,}')
	data = "no"

	if patron.search(var.get("pass1")):
		data = "ok"
	
	return StreamingHttpResponse(data)

def register_pass2_ajax(request):
	var = req_vars(request)
	data = "no"

	if var.get("pass1")==var.get("pass2"):
		data = "ok"
	
	return StreamingHttpResponse(data)

def register_nombre_ajax(request):
	var = req_vars(request)
	patron = re.compile('[a-z|A-Z]{3,}')
	data = "no"

	if patron.search(var.get("nombre")):
		data = "ok"
	
	return StreamingHttpResponse(data)

def register_lastname_ajax(request):
	var = req_vars(request)
	patron = re.compile('[a-z|A-Z]{3,}\s[a-z|A-Z]{3,}')
	data = "no"

	if patron.search(var.get("lastname")):
		data = "ok"
	
	return StreamingHttpResponse(data)

def register_email_ajax(request):
	var = req_vars(request)
	patron = re.compile('.+@[a-z|A-Z]{3,}\.[a-z|A-Z]+')
	data = "no"

	if patron.search(var.get("email")):
		data = "ok"

	if Usuario.objects.filter(email=var.get("email")).count()!=0:
		data = "repetido"
		return StreamingHttpResponse(data)
	
	return StreamingHttpResponse(data)

def recovery(request, validate_number):
	var = req_vars(request)

	try:
		usuario = Usuario.objects.get(codigo_validacion=validate_number)
	except:
		usuario = None

	if usuario:
		if check_submit(request, 'recovery_set_password'):
			elements = ['recovery_password1', 'recovery_password2']
			if check_request(request, elements):
				if var.get('recovery_password1') == var.get('recovery_password2'):
					usuario.set_password(var.get('recovery_password1'))
					usuario.codigo_validacion = ""
					usuario.save()
					msg = gettext("You have successfully changed your password")
					estado = gettext("Successfull")
					context = {'msg':msg, 'estado':estado}
					return render(request, 'validate.html', context)
				else:
					msg = gettext("Password must be equals")
					estado = gettext("Error")
					context = {'msg':msg, 'estado':estado}
					return render(request, 'validate.html', context)
	else:
		msg = gettext("You can not reset the password")
		estado = gettext("Error")
		context = {'msg':msg, 'estado':estado}
		return render(request, 'validate.html', context)

	return render(request, 'recovery.html', "")

def success_registered(request):

	context = {}
	return render(request, 'success_registered.html', context)

def validate(request, validate_number):
	try:
		usuario = Usuario.objects.get(codigo_validacion=validate_number)
	except:
		usuario = None

	if usuario:
		msg = gettext("You have successfully validated email ") + usuario.email
		estado = gettext("Successfull")
		usuario.is_active = True
		usuario.codigo_validacion = ""
		usuario.save()
	else:
		msg = gettext("A error occurred when validating the email. That email will already validated?")
		estado = gettext("Error")

	context = {'msg':msg, 'estado':estado}
	return render(request, 'validate.html', context)

def orders(request):
	var = req_vars(request)
	session = my_session(request.session)
	lista_pedidos = [] # factura, lista_lineas, no eliminado
	lista_marcados = session.get_if_exist('lista_marcados',[]) # factura_id

	if request.method == "POST":
		elements = ['orders_checked']
		if check_request(request, elements):
			factura = Factura.objects.get(id=var.get('orders_checked'))
			if not factura.pedido_entregado:
				lista_marcados.insert(0, factura.id)
				factura.pedido_entregado = True
				factura.save()

				texto = load_from_configuration_file('TextToCompleteOrder')
				texto = texto.replace("<N>",str(factura.id))

				engine = pyttsx.init()
				engine.setProperty('rate', 100)
				engine.setProperty('voice', "spanish-latin-american")
				engine.say(texto)

				#thread.start_new_thread( speech.say, ( texto, ) )

				thread.start_new_thread( engine.runAndWait, (  ) )
			else:
				lista_marcados.remove(factura.id)
			session.put("lista_marcados", lista_marcados)

	for factura in Factura.objects.filter(pedido_entregado=False):
		lista_pedidos.append((factura, Linea.objects.filter(factura=factura), False))

	for factura_id in lista_marcados:
		factura = Factura.objects.get(id=factura_id)
		lista_pedidos.insert(0,(factura, Linea.objects.filter(factura=factura), True))

	permisos_necesarios = ['Administration', 'Orders']
	context = {'dir':'orders', 'lista_pedidos':lista_pedidos}
	return do_login(request, 'orders.html', context, permisos_necesarios)

def orders_ajax(request):
	data = "no"
	var = req_vars(request)
	lista_facturas = Factura.objects.filter(pedido_entregado=False)
	lista_pedidos_nuevos = []
	lista_pedidos = var.get("pedidos").split("-")

	for factura in lista_facturas:
		encontrado = False
		for item in lista_pedidos:
			
			if str(factura.id) == item:
				print str(factura.id) + " == " + item
				encontrado = True

		if not encontrado:
			lista_pedidos_nuevos.append((factura, Linea.objects.filter(factura=factura), False))

	if lista_pedidos_nuevos:
		data = lista_pedidos_nuevos
	return StreamingHttpResponse(data)

def event_management(request):
	var = req_vars(request)
	session = my_session(request.session)
	dt_init = DateTime_Controller("init")
	dt_finish = DateTime_Controller("finish")

	# Cargar desde la sesión
	tag_actual = session.get_if_exist('tag_actual_group', "tab1")

	if check_submit(request, 'event_create'):
		tag_actual = "tab1"
		elements = ['event_create_name','event_create_init_hour','event_create_init_date','event_create_finish_hour','event_create_finish_date']
		if check_request(request, elements):
			dt_init.set_from_datepicker(var.get('event_create_init_date'), var.get('event_create_init_hour'))
			dt_finish.set_from_datepicker(var.get('event_create_finish_date'), var.get('event_create_finish_hour'))

			evento = Evento(nombre=var.get("event_create_name"), fecha_inicio=dt_init.get_to_djangodb_datetime(), fecha_final=dt_finish.get_to_djangodb_datetime())
			evento.save()
			
	if check_submit(request, 'event_delete'):
		tag_actual = "tab2"
		elements = ['event_delete_name_temp']
		if check_request(request, elements):
			evento = Evento.objects.get(nombre=var.get("event_delete_name_temp"))
			evento.delete()


	# Carga de datos en la plantilla
	lista_eventos = Evento.objects.all()

	# Guardar en la sesion
	session.put('tag_actual_group', tag_actual)

	permisos_necesarios = ['Administration']
	context = {'dir':'event_management','tag_actual':tag_actual,'lista_eventos':lista_eventos}
	return do_login(request, 'event_management.html', context, permisos_necesarios)

































def plantilla(request):

	permisos_necesarios = ['Administration']
	context = {}
	return do_login(request, 'administration.html', context, permisos_necesarios)
