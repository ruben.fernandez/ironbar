from django.contrib import admin
from barra.models import *

# Register your models here.

admin.site.register(Usuario)
admin.site.register(Permiso)
admin.site.register(Producto)
admin.site.register(Factura)
admin.site.register(Tipo_Precio)
admin.site.register(Precio)
admin.site.register(Linea)
admin.site.register(Grupo)
admin.site.register(Permiso_Grupo)
admin.site.register(Permiso_Usuario)
admin.site.register(Evento)