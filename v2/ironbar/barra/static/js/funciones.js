function show(id_objeto) {
	document.getElementById(id_objeto).style.display = 'block';
}

function hide(id_objeto) {
	document.getElementById(id_objeto).style.display = 'None';
}

function js_error(flag) {
	if (flag != 1) {
		return "has-error";
	} else {
		return "has-success";
	}
}

function check_same_pass() {
	var pass1 = document.getElementById("register_pass1").value;
	var pass2 = document.getElementById("register_pass2").value;
	if (pass1!=pass2) {
		d = document.getElementById("register_password_form");
		d.className = d.className + " has-error";
		return false;
	}
	return true;
}

function check_this_pass(input) {
	if (input.value != document.getElementById('register_pass1').value) {
    	input.setCustomValidity('Password must match.');
  	} else {
    	input.setCustomValidity('');
  	}
}

function var_to_var(var1, id_var2) {
	document.getElementById(id_var2).setAttribute("value", var1);
}

function var_to_var_2(var1, id_var2, var3, id_var4) {
	document.getElementById(id_var2).setAttribute("value", var1);
	document.getElementById(id_var4).setAttribute("value", var3);
}

function var_to_text(var1, div_id) {
	var d = document.getElementById(div_id);
	var text = document.createTextNode(var1);
	d.appendChild(text);
}

function prize_copy_edit(var1, id_var2, var3, id_var4, var5, id_var6) {
	document.getElementById(id_var2).setAttribute("value", var1);
	document.getElementById(id_var6).setAttribute("value", var5);
	var contenido = document.createTextNode(var3);
	var text = document.getElementById(id_var4)
	while (text.firstChild) {
    	text.removeChild(text.firstChild);
	}
	text.appendChild(contenido);
}

function select_to_var(id_select, id_var) {
	var select = document.getElementById(id_select);
	var seleccionado = select.options[select.selectedIndex].text

	document.getElementById(id_var).setAttribute("value", seleccionado);
}

function select_to_var_2(id_select, id_var, id_select2, id_var2) {
	var select = document.getElementById(id_select);
	var seleccionado = select.options[select.selectedIndex].text
	var select2 = document.getElementById(id_select2);
	var listaSeleccionado = new String("");

	for (var i = 0; i < select2.options.length; i++)
    if (select2.options[ i ].selected)
  		listaSeleccionado = listaSeleccionado + ";;;" + select2.options[ i ].value;

	document.getElementById(id_var).setAttribute("value", seleccionado);
	document.getElementById(id_var2).setAttribute("value", listaSeleccionado);
}

function submitform(form_id) {
	document.getElementById(form_id).submit();
}

function group_select_user(texto, id_div) {
	var contenido = document.createTextNode(texto);
	var div = document.getElementById(id_div);
	div.appendChild(contenido);
}

function clean_div(id_div) {
	var div = document.getElementById(id_div);
	while (div.hasChildNodes()) {
		div.removeChild(div.lastChild);
	}
}

function select_radius(id_radio_label, id_radio) {
	var d = document.getElementById(id_radio_label);
	d.className = d.className + " checked";
	d = document.getElementById(id_radio);
	d.checked = true;
}

function unselect_radius(id_radio_label, id_radio) {
	document.getElementById(id_radio_label).classList.remove("checked");
	document.getElementById(id_radio).checked = false;
}

function enable_element(id_element) {
	var d = document.getElementById(id_element);
	d.removeAttribute("disabled");
}

function delete_class(id_element, clase) {
  document.getElementById(id_element).classList.remove(clase);
}

function add_class(id_element, clase) {
	var d = document.getElementById(id_element);
	if (d.className == "") {
		d.className = clase;
	} else {
		d.className = d.className + " " + clase;
	}
	//console.log("hola");
}

function select_select(id_select, option) {
	var d = document.getElementById(id_select);
	for (var i=0; i<d.length; i++) {
		var p = document.getElementById('prueba_edit');
		p.setAttribute("value",p.value+i);
		if (d.options[i].text == option) {
			d.options[i].setAttribute("selected","selected");
			p.setAttribute("value",p.value+"=");
		}
	}
}	

function check_in_box(id_checkbox) {
	var d = document.getElementById(id_checkbox);
	d.checked = true;
}

function check_out_box(id_checkbox) {
	var d = document.getElementById(id_checkbox);
	d.checked = false;
}

function test_reusable(opcion1, opcion2) {
	var d = document.getElementById(opcion1);
	d.setAttribute("value", opcion2);
}

function uncheck_class(id_element) {
	document.getElementById(id_element).className = document.getElementById(id_element).className.replace( /\bchecked\b/, '' );
}

function unselect_row_class(id_element) {
	document.getElementById(id_element).className = document.getElementById(id_element).className.replace( /\bselected-row\b/, '' );
}

function console_print(text) {
	console.log(text);	
}

function add_to_div(sumando1, text_id_sumando2, id_div_solucion) {
	sol = parseFloat(sumando1)

	console.log(sol);
}

function restar(sustraendo, minuendo_id, resta_id) {
	var minuendo = document.getElementById("" + minuendo_id + "").value.replace(/\n/g, '<br>');
	var resta = minuendo - sustraendo;
  	document.getElementById(resta_id).innerHTML = resta + " €"; 
}

function do_focus(id_div) {
	document.getElementById(id_div).setfocus();
}

function remove_attribute(id_div, attribute) {
	document.getElementById(id_div).removeAttribute(attribute);
}

function add_attribute(id_div, attribute) {
	document.getElementById(id_div).setAttribute(attribute,"true")
}

function delete_has_error(id_element) {
	document.getElementById(id_element).className = document.getElementById(id_element).className.replace( /\bhas-error\b/, '' );
}

function delete_has_success(id_element) {
	document.getElementById(id_element).className = document.getElementById(id_element).className.replace( /\bhas-success\b/, '' );
}

function sumar_liabilities(element) {
	var pulsado = element.getAttribute("pulsado");
	var importe = element.getAttribute("importe");
	var total = document.getElementById("total_js");
	var total_temp = document.getElementById("total_js_temp");
	var checkbox_big = document.getElementById("liabilities_checkbox_big");
	var checkboxes = document.getElementsByName("botones_con_importe");
	var suma = 0;

	if (pulsado=="no") {
		suma = parseFloat(importe.replace(",",".")) + parseFloat(total_temp.value.replace(",","."));
		var todo_si = true
		for (i=0; i<checkboxes.length; i++) {
	    	if (checkboxes[i].getAttribute("pulsado")=="no") {
	    		todo_si = false
	    	}
		}
		if (todo_si) {
			checkbox_big.setAttribute("pulsado", "si");
		}
		element.setAttribute("pulsado", "si")
	} else {
		suma = parseFloat(total_temp.value.replace(",",".")) - parseFloat(importe.replace(",","."));
		element.setAttribute("pulsado", "no")
		checkbox_big.setAttribute("pulsado", "no")
	}

	total_temp.setAttribute("value", suma);

	while (total.hasChildNodes()) {
		total.removeChild(total.lastChild);
	}
	var text = document.createTextNode(total_temp.value);
	total.appendChild(text);

}

function sumar_liabilities_todos(element) {
	var total = document.getElementById("total_js");
	var total_temp = document.getElementById("total_js_temp");
	var pulsado = element.getAttribute("pulsado");

	var checkboxes = document.getElementsByName("botones_con_importe");
	var importe = 0
	if (pulsado=="no") {
		for (i=0; i<checkboxes.length; i++) {
	    	importe = importe + parseFloat(checkboxes[i].getAttribute("importe").replace(",","."));
	    	checkboxes[i].setAttribute("pulsado", "si")
		}
		element.setAttribute("pulsado", "si")
	} else {
		for (i=0; i<checkboxes.length; i++) {
	    	checkboxes[i].setAttribute("pulsado", "no")
		}
		element.setAttribute("pulsado", "no")
	}

	total_temp.setAttribute("value", importe)

	while (total.hasChildNodes()) {
		total.removeChild(total.lastChild);
	}
	var text = document.createTextNode(importe);
	total.appendChild(text);

}











